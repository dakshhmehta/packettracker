﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using PacketSniffer.UI;
using PacketSniffer.DataBase;


namespace PacketSniffer
{
    static class Program
    {
        public static PMUserEntity LoginUser = null;
        public static MainForm MainForm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DataBase.SQLDB.Open();

            LoginForm form = new LoginForm();

            if (form.ShowDialog() == DialogResult.OK)
            {
                MainForm = new MainForm();
                Application.Run(MainForm);

                DB01PMUser.UpdatePMUserLogout(LoginUser.pmUserIndex);
            }

           
            DataBase.SQLDB.Close();
        }
    }
}
