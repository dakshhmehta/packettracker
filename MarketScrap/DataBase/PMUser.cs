﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;

using PacketSniffer;

namespace PacketSniffer.DataBase
{
    public enum EAdminRole
    {
        User = 1,
        Administrator = 0,
    }
    public class PMUserEntity
    {
        #region Fields
        public int pmUserIndex;
        public String pmUserName;
        public int role;
        public string password;
        public string LoginTime;
        public string LogoutTime;
        public string Email;

        #endregion

        #region constructor
        public PMUserEntity()
        {
            pmUserIndex = -1;
            pmUserName = "";
            role = 0;
            password = "";
        }

        public PMUserEntity(string pmUserNameSet, int roleSet, string passwordSet)
        {
            pmUserIndex = -1;
            pmUserName = pmUserNameSet;
            role = roleSet;
            password = passwordSet;

            /////////////////////////////
        }
        public PMUserEntity(PMUserEntity clone)
        {
            Clone(clone);
        }

        public void Clone(PMUserEntity clone)
        {
            pmUserIndex = clone.pmUserIndex;
            pmUserName = clone.pmUserName;
            role = clone.role;
            password = clone.password;
        }
        #endregion
    }
    public class DB01PMUser
    {
        #region consts

        private const string DB_TABLENAME = "TPMUser";
        private const string F_INDEX = "pmUserIndex";
        private const string F_NAME = "Name";
        private const string F_ROLE = "Role";
        private const string F_PASSWORD = "Password";
        private const string F_LOGIN = "LoginTime";
        private const string F_LOGOUT = "LogoutTime";
        private const string F_EMAIL = "Email";
        #endregion

        #region check - exsitance

        public static bool IsExist(string nameID)
        {
            string sql = "select count(*) from " + DB_TABLENAME + " where " + F_NAME + "='" + nameID + "'";
            int count = (int)SQLDB.ExecuteScalar(sql);
            return count > 0;
        }

        public static bool IsExist(int ID)
        {
            string sql = "select count(*) from " + DB_TABLENAME + " where " + F_INDEX + "=" + ID;
            int count = (int)SQLDB.ExecuteScalar(sql);
            return count > 0;
        }

        #endregion

        #region function - insert
        public static void InsertPMUserEntity(PMUserEntity entity)
        {
            try
            {
                string sql;

                sql = string.Format("insert into " + DB_TABLENAME + "([" + F_NAME + "],[" + F_ROLE + "],[" + F_PASSWORD + "],[" + F_LOGIN + "],[" + F_LOGOUT + "],[" + F_EMAIL + "]) " +
                                    "values('{0}',{1},{2},'{3}','{4}','{5}')",
                                    entity.pmUserName, entity.role, entity.password.GetHashCode().ToString(), entity.LoginTime, entity.LogoutTime, entity.Email);
                SQLDB.ExecuteNonQuery(sql);

            }
            catch (Exception err)
            {
                SQLDB.RollbackTransaction();
                throw err;
            }
        }
        #endregion

        #region function - get - entity

        public static ArrayList LoadPMUsers(string likeName)
        {
            string sql = string.Format("select * from " + DB_TABLENAME);
            if (!string.IsNullOrEmpty(likeName)) sql += string.Format(" where [" + F_NAME + "] = '{0}'", likeName);
            DataTable dt = SQLDB.ExecuteTable(sql);
            ArrayList list = new ArrayList(dt.Rows.Count);
            foreach (DataRow rw in dt.Rows)
            {
                PMUserEntity entity = GetUserFromDatarow(rw);
                list.Add(entity);
            }
            return list;
        }

        public static int GetPMAdmins()
        {
            string sql = string.Format("select * from " + DB_TABLENAME + " where [" + F_ROLE + "]={0}", (int)EAdminRole.Administrator);
            DataTable dt = SQLDB.ExecuteTable(sql);
            return dt.Rows.Count;
        }
        public static PMUserEntity GetPMUserEntity(string name, int role)
        {
            string sql = string.Format("Select * from " + DB_TABLENAME + " where [" + F_NAME + "]='{0}' AND [" + F_ROLE + "]={1}", name, role);
            DataTable dt = SQLDB.ExecuteTable(sql);
            if (dt.Rows.Count > 0)
                return GetUserFromDatarow(dt.Rows[0]);
            else
                return null;
        }
        public static PMUserEntity GetPMUserEntity(int userID)
        {
            string sql = string.Format("select * from " + DB_TABLENAME + " where [" + F_INDEX + "]={0}", userID);
            DataTable dt = SQLDB.ExecuteTable(sql);
            if (dt.Rows.Count > 0)
                return GetUserFromDatarow(dt.Rows[0]);
            else
                return null;
        }
        private static PMUserEntity GetUserFromDatarow(DataRow rw)
        {
            PMUserEntity entity = new PMUserEntity();
            entity.pmUserIndex = (int)rw[F_INDEX];
            entity.pmUserName = rw[F_NAME].ToString();
            entity.role = (int)rw[F_ROLE];
            entity.password = rw[F_PASSWORD].ToString();
            entity.LoginTime = Convert.ToString(rw[F_LOGIN]);
            entity.LogoutTime = Convert.ToString(rw[F_LOGOUT]);
            return entity;
        }
        #endregion

        #region function - update
        public static int UpdatePMUserEntity(PMUserEntity entity)
        {
            int result = 1;
            bool state = IsExist(entity.pmUserIndex);
            if (state == true)
            {
                try
                {
                    string sql;
                    string password = entity.password;
                    if (Program.MainForm == null || Program.MainForm.UserInformationForm.PwdChange)
                        password = password.GetHashCode().ToString();

                    sql = string.Format("update " + DB_TABLENAME + " set [" + F_NAME + "]='{1}',[" + F_ROLE + "]={2},[" + F_PASSWORD + "]='{3}',[" + F_LOGIN + "]='{4}',[" + F_LOGOUT + "]='{5}' where [" + F_INDEX + "]={0}",
                                        entity.pmUserIndex, entity.pmUserName, entity.role, password, entity.LoginTime, entity.LogoutTime);
                    SQLDB.ExecuteNonQuery(sql);
                }
                catch (Exception err)
                {
                    SQLDB.RollbackTransaction();
                    throw err;
                }
            }
            else
            {
                result = 0;
            }
            return result;
        }
        public static int UpdatePMUserLogout(int ID)
        {
            int result = 1;
            bool state = IsExist(ID);
            if (state == true)
            {
                try
                {
                    string sql;

                    sql = string.Format("update " + DB_TABLENAME + " set [" + F_LOGOUT + "]='{1}' where [" + F_INDEX + "]={0}",
                                        ID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    SQLDB.ExecuteNonQuery(sql);
                }
                catch (Exception err)
                {
                    SQLDB.RollbackTransaction();
                    throw err;
                }
            }
            else
            {
                result = 0;
            }
            return result;
        }

        #endregion

        #region function - remove

        public static int RemovePMUserEntity(int Index)
        {
            int result = 1;
            bool state = IsExist(Index);
            if (state == true)
            {
                try
                {
                    string sql = string.Format("delete from " + DB_TABLENAME + " where [" + F_INDEX + "]={0}", Index);
                    SQLDB.ExecuteNonQuery(sql);
                }
                catch (Exception err)
                {
                    SQLDB.RollbackTransaction();
                    throw err;
                }
            }
            else
            {
                result = 0;
            }
            return result;
        }


        public static int RemovePMUserEntity(string userName, int role)
        {
            int result = 1;
            bool state = IsExist(userName);
            if (state == true)
            {
                try
                {
                    string sql = string.Format("delete from " + DB_TABLENAME + " where [" + F_NAME + "]='{0}' AND [" + F_ROLE + "]={1}", userName, role);
                    SQLDB.ExecuteNonQuery(sql);
                }
                catch (Exception err)
                {
                    SQLDB.RollbackTransaction();
                    throw err;
                }
            }
            else
            {
                result = 0;
            }
            return result;
        }
        #endregion 
    }


}
