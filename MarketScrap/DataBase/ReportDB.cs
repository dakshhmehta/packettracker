﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

using PacketSniffer.UI;

namespace PacketSniffer.DataBase
{
    public class ReportEntity
    {
        #region Fields

        public int ID = -1;
        public int SerialNo=-1;
        public string Date = DateTime.Today.ToString("yyyy/MM/dd");
        public string Time = DateTime.Now.ToShortTimeString();
        public string SourceIP = "0.0.0.0";
        public string DestinationIP = "0.0.0.0";
        public Protocol Type = Protocol.TCP;
        //public float Duration = 0;
        public long UploadData = 0;
        public long DownloadData = 0;
        public int TotalPackets = 0;
        public int ManagementPackets = 0;
        public int DataPackets = 0;
        public int ControlPackets = 0;
        public int OtherPackets = 0;
        public string SourceMACAddress = String.Empty;
        public string DestinationMACAddress = String.Empty;
        public string DestinationName = String.Empty;
        public bool IsWireless = false;
        #endregion

        #region constructor
        public ReportEntity()
        {
            SerialNo    = -1;
            Date = 
            Time = DateTime.Now.ToShortTimeString();
        }
        

        public ReportEntity(ReportEntity clone)
        {
            Clone(clone);
        }

        public void Clone(ReportEntity clone)
        {
            this.SerialNo = clone.SerialNo;
            this.Date = clone.Date;
            this.Time = clone.Time;
            this.ControlPackets = clone.ControlPackets;
            this.DataPackets = clone.DataPackets;
            this.DestinationIP = clone.DestinationIP;
            this.DownloadData = clone.DownloadData;
            //this.Duration = clone.Duration;
            this.ManagementPackets = clone.ManagementPackets;
            this.OtherPackets = clone.OtherPackets;
            this.SerialNo = clone.SerialNo;
            this.SourceIP = clone.SourceIP;
            this.TotalPackets = clone.TotalPackets;
            this.Type = clone.Type;
            this.UploadData = clone.UploadData;
            this.SourceMACAddress = clone.SourceMACAddress;
            this.DestinationMACAddress = clone.DestinationMACAddress;
        }
        #endregion
    }
    public class ReportDB
    {
        #region consts

        private const string TABLENAME   = "Reports";
        private const string F_INDEX = "ID";
        private const string F_SrNO = "SerialNo";
        private const string F_DATE ="Date";
        private const string F_TIME         = "Time";
        private const string F_SOURCEIP      = "SourceIP";
        private const string F_DESTIP = "DestinationIP";
        private const string F_SOURCEMAC = "SourceMAC";
        private const string F_DESTMAC = "DestinationMAC";
        private const string F_DESTNAME = "DestinationName";
        private const string F_TYPE = "Type";
        //private const string F_DURATION = "Duration";
        private const string F_UPLOADDATA = "UploadData";
        private const string F_DOWNLOADDATA = "DownloadData";
        private const string F_TOTALPACKETs = "TotalPackets";
        private const string F_ManagementPackets = "ManagementPackets";
        private const string F_DataPackets = "DataPackets";
        private const string F_ControlPackets = "ControlPackets";
        private const string F_OtherPackets = "OtherPackets";
        private const string F_ISWIRELESS = "IsWireless";

        #endregion

        #region check - exsitance

        private static bool IsExist(int ID)
        {
            string sql = "select count(*) from " + TABLENAME + " where " + F_INDEX + "=" + ID;
            int count = (int)SQLDB.ExecuteScalar(sql);
            return count > 0;
        }


        public static bool IsExist(ReportEntity log)
        {
            string sql = string.Format("select * from " + TABLENAME + " where ([" + F_SrNO + "]= {0}  AND [{1}]='{2}')", log.SerialNo, F_DATE, log.Date);
            DataTable dt = SQLDB.ExecuteTable(sql);
            return (dt.Rows.Count > 0);
        }

        #endregion

        #region function - insert
        
        public static int InsertLogEntity(ReportEntity entity)
        {
            int result = 1;
             
                try
                {
                    string sql="",date_t = "";
                    sql += "insert into " + TABLENAME +"(["+F_SrNO+"],["+
                                                            F_DATE + "],[" +
                                                            F_TIME + "],[" +
                                                            F_SOURCEIP + "],[" +
                                                            F_DESTIP + "],[" +
                                                            F_TYPE + "],[" +
                                                            F_UPLOADDATA + "],[" +
                                                            F_DOWNLOADDATA + "],[" +
                                                            F_TOTALPACKETs + "],[" +
                                                            F_ManagementPackets + "],[" +
                                                            F_DataPackets + "],[" +
                                                            F_ControlPackets + "],[" +
                                                            F_OtherPackets + "],[" +
                                                            F_SOURCEMAC + "],[" +
                                                            F_DESTMAC + "],[" +
                                                            F_DESTNAME + "],[" +
                                                            F_ISWIRELESS + "]) " +
                                                  " values(";
                sql += string.Format("'{0}', '{1}','{2}', '{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}',{16}  )",
                     entity.SerialNo, entity.Date, entity.Time, entity.SourceIP, entity.DestinationIP,
                    entity.Type, entity.UploadData, entity.DownloadData,
                    entity.TotalPackets, entity.ManagementPackets, entity.DataPackets, entity.ControlPackets, entity.OtherPackets,
                    entity.SourceMACAddress, entity.DestinationMACAddress, entity.DestinationName, entity.IsWireless);                    

                    SQLDB.ExecuteNonQuery(sql);
                }
                catch (Exception err)
                {
                    SQLDB.RollbackTransaction();
                }
             
            return result;
        }
        #endregion

        #region function - get - entity

        public static ArrayList SearchAll()
        {
            string sql = string.Format("select * from " + TABLENAME);
            DataTable dt = SQLDB.ExecuteTable(sql); 

            ArrayList list = new ArrayList(dt.Rows.Count);
            foreach (DataRow rw in dt.Rows)
            {
                ReportEntity entity = GetLogFromDatarow(rw);
                list.Add(entity);
            }
            return list;
        }

        public static List<ReportEntity> SearchForDate(DateTime beginDate, DateTime endDate)
        {
            string sql = string.Format("select * from " + TABLENAME + " where ([" + F_DATE + "]>='{0}' and [" + F_DATE + "]<='{1}')", beginDate.ToString("yyyy/MM/dd"), endDate.ToString("yyyy/MM/dd"));
            DataTable dt = SQLDB.ExecuteTable(sql);
            List<ReportEntity> list = new  List<ReportEntity>();
            foreach (DataRow rw in dt.Rows)
            {
                ReportEntity entity = GetLogFromDatarow(rw);
                list.Add(entity);
            }
            return list;
        }
         
        public static ReportEntity GetLogEntity(int logID)
        {
            string sql = string.Format("select * from " + TABLENAME + " where [" + F_INDEX + "]={0}", logID);
            DataTable dt = SQLDB.ExecuteTable(sql);
            if (dt.Rows.Count > 0)
                return GetLogFromDatarow(dt.Rows[0]);
            else
                return null;

        }

        private static ReportEntity GetLogFromDatarow(DataRow rw)
        {
            ReportEntity entity = new ReportEntity();
            try
            {
                entity.ID = (int)rw[F_INDEX];
                entity.SerialNo = (int)rw[F_SrNO];
                entity.Date   = (string)rw[F_DATE];
                entity.Time      = (string)rw[F_TIME];
                entity.SourceIP = (string)rw[F_SOURCEIP];
                entity.DestinationIP = (string)rw[F_DESTIP];
                entity.Type = (Protocol)Enum.Parse(typeof(Protocol), (string)rw[F_TYPE]);
                entity.UploadData = Convert.ToInt64(rw[F_UPLOADDATA]);
                entity.DownloadData = Convert.ToInt64(rw[F_DOWNLOADDATA]);
                entity.TotalPackets = Convert.ToInt32(rw[F_TOTALPACKETs]);
                entity.ManagementPackets = Convert.ToInt32(rw[F_ManagementPackets]);
                entity.DataPackets = Convert.ToInt32(rw[F_DataPackets]);
                entity.ControlPackets = Convert.ToInt32(rw[F_ControlPackets]);
                entity.OtherPackets = Convert.ToInt32(rw[F_OtherPackets]);
                entity.SourceMACAddress = (string)rw[F_SOURCEMAC];
                entity.DestinationMACAddress = (string)rw[F_DESTMAC];
                entity.DestinationName = (string)rw[F_DESTNAME];
                entity.IsWireless = (bool) rw[F_ISWIRELESS];
            }
            catch { }
            return entity;
        }
        #endregion

        #region function - update
        public static int UpdateLogEntity(ReportEntity entity)
        {
            int result = 1;
            if(IsExist(entity.SerialNo))
            {
                 
                    try
                    {
                        string sql;
                        sql = string.Format("update " + TABLENAME + " set [{0}]='{1}',[{2}]='{3}',[{4}]='{5}' ,[{6}]='{7}', [{8}]='{9}',[{10}]='{11}',[{12}]='{13}' ,[{14}]='{15}'  where [" + F_SrNO + "]=" + entity.SerialNo+" AND [" + F_DATE + "]='" + entity.Date +"'",
                                            F_TYPE, entity.Type,
                                            F_UPLOADDATA,entity.UploadData,
                                            F_DOWNLOADDATA, entity.DownloadData, 
                                            F_TOTALPACKETs, entity.TotalPackets,
                                            F_ManagementPackets,entity.ManagementPackets,
                                            F_DataPackets,entity.DataPackets,
                                            F_ControlPackets, entity.ControlPackets,
                                            F_OtherPackets,entity.OtherPackets);
                        SQLDB.ExecuteNonQuery(sql);
                    }
                    catch (Exception err)
                    {
                        SQLDB.RollbackTransaction();
                        throw err;
                    }
                
            }
            else
            {
                result =  -1;
            }
            return result;
        }

        #endregion

        #region function - remove
        public static int RemoveLogEntity(int Index)
        {
            int result = 1;
            if (IsExist(Index))
            {
                try
                {
                    string sql = string.Format("delete from " + TABLENAME + " where [" + F_INDEX + "]={0}", Index);
                    SQLDB.ExecuteNonQuery(sql);
                }
                catch (Exception err)
                {
                    SQLDB.RollbackTransaction();
                    throw err;
                }
            }
            else
            {
                result =  -1;
            }
            return result;
        }

        public static int RemoveAllLogEntity()
        {
            int result = 1;
            try
            {
                string sql = string.Format("delete from " + TABLENAME);
                SQLDB.ExecuteNonQuery(sql);
            }
            catch (Exception err)
            {
                SQLDB.RollbackTransaction();
                throw err;
            }
            return result;
        }
      
        #endregion 
    }
}
