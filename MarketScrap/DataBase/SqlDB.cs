﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Globalization;
using System.Configuration;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace PacketSniffer.DataBase
{
    public static  class SQLDB
    {
        public const string SUPER_ADMINISTRATOR = "Administrator";
        private const string DATABASE_CLOSE = "Database is closed.";
         
        public static int tableIndex = -1;

        private static DbConnection mConnection;
        private static DbTransaction mTransaction = null;
        private static string mConnectionString, mProviderName;

         
        public static bool Open()
        {
            try
            {
                mConnectionString = ConfigurationManager.ConnectionStrings["OleDB"].ConnectionString /*+ "Jet OLEDB:Database Password=;"*/;
                mProviderName = ConfigurationManager.ConnectionStrings["OleDB"].ProviderName;

                if (mConnection != null && mConnection.State != ConnectionState.Closed)
                {
                    mConnection.Close();
                }
                mConnection = null;

                try
                {
                    DbProviderFactory providerFactory = DbProviderFactories.GetFactory(mProviderName);
                    mConnection = providerFactory.CreateConnection();
                }
                catch
                {
                    throw new NotImplementedException(string.Format("Provider:{0} is not implemented.", mProviderName));
                }

                mConnection.ConnectionString = mConnectionString;
                mConnection.Open();
                return true;
            }
            catch (SqlException e)
            {
                if (mConnection != null)
                    mConnection.Dispose();
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public static bool Close()
        {
            try
            {
                if (mConnection != null && mConnection.State != ConnectionState.Closed)
                {
                    mTransaction = null;
                    mConnection.Close();
                    return true;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return false;
        }
          
        public static DataTable ExecuteTable(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                if (mConnection == null) throw new Exception(DATABASE_CLOSE);
                DbProviderFactory dataFactory = DbProviderFactories.GetFactory(mProviderName);

                DbDataAdapter da = dataFactory.CreateDataAdapter();
                da.SelectCommand = mConnection.CreateCommand();
                da.SelectCommand.CommandText = sql;
                da.SelectCommand.Transaction = mTransaction;

                DbDataReader reader = da.SelectCommand.ExecuteReader();
                dt.Load(reader);
                //da.Fill(dt);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return dt;
        }

        /// <summary>
        /// Non-SQL query execution method
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>The number of affected rows</returns>
        /// <exception cref="System.Data.Common.DbException">When the database returns an error</exception>
        /// <exception cref="System.InvalidOperationException">Connection does not exist or is not open, or the transaction is not registered</exception>
        public static int ExecuteNonQuery(string sql)
        {
            if (mConnection == null) throw new Exception(DATABASE_CLOSE);
            DbCommand command = mConnection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = sql;
            command.Transaction = mTransaction;

            return command.ExecuteNonQuery();
        }


        /// <summary>
        /// The first column of the first row in the result set of。
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static object ExecuteScalar(string sql)
        {
            if (mConnection == null) throw new Exception(DATABASE_CLOSE);
            DbCommand command = mConnection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = sql;
            command.Transaction = mTransaction;

            return command.ExecuteScalar();
        }

        /// <summary>
        /// Database transaction rollback。
        /// </summary>
        /// <exception cref="System.Exception">An error occurred while attempting to commit a transaction。</exception>
        /// <exception cref="System.InvalidOperationException">Transaction is committed or rolled back, or the connection has been disconnected。</exception>
        public static void RollbackTransaction()
        {
            if (mTransaction != null) mTransaction.Rollback();
            mTransaction = null;
        }

    }
}