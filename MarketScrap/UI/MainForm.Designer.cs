﻿
namespace PacketSniffer.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabPanel = new System.Windows.Forms.Panel();
            this.imageListMainTabLeft = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.imageListMainTabRight = new System.Windows.Forms.ImageList(this.components);
            this.imageListMainTabMiddle = new System.Windows.Forms.ImageList(this.components);
            this.netUpdator = new System.Windows.Forms.Timer(this.components);
            this.imageListLogout = new System.Windows.Forms.ImageList(this.components);
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.btnUsersTab = new PacketSniffer.UI.TabMenuButton();
            this.btnLogManagerTab = new PacketSniffer.UI.TabMenuButton();
            this.btnReportTab = new PacketSniffer.UI.TabMenuButton();
            this.btnLiveMonitorTab = new PacketSniffer.UI.TabMenuButton();
            this.btnLogout = new PacketSniffer.UI.IconButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPanel
            // 
            this.tabPanel.BackColor = System.Drawing.Color.Transparent;
            this.tabPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.tabPanel, "tabPanel");
            this.tabPanel.Name = "tabPanel";
            // 
            // imageListMainTabLeft
            // 
            this.imageListMainTabLeft.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListMainTabLeft.ImageStream")));
            this.imageListMainTabLeft.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListMainTabLeft.Images.SetKeyName(0, "main_tab_normal1.png");
            this.imageListMainTabLeft.Images.SetKeyName(1, "main_tab_highlight1.png");
            this.imageListMainTabLeft.Images.SetKeyName(2, "main_tab_clicked1.png");
            this.imageListMainTabLeft.Images.SetKeyName(3, "main_tab_clicked1.png");
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // imageListMainTabRight
            // 
            this.imageListMainTabRight.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListMainTabRight.ImageStream")));
            this.imageListMainTabRight.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListMainTabRight.Images.SetKeyName(0, "main_tab_normal3.png");
            this.imageListMainTabRight.Images.SetKeyName(1, "main_tab_highlight3.png");
            this.imageListMainTabRight.Images.SetKeyName(2, "main_tab_clicked3.png");
            this.imageListMainTabRight.Images.SetKeyName(3, "main_tab_clicked3.png");
            // 
            // imageListMainTabMiddle
            // 
            this.imageListMainTabMiddle.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListMainTabMiddle.ImageStream")));
            this.imageListMainTabMiddle.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListMainTabMiddle.Images.SetKeyName(0, "main_tab_normal2.png");
            this.imageListMainTabMiddle.Images.SetKeyName(1, "main_tab_highlight2.png");
            this.imageListMainTabMiddle.Images.SetKeyName(2, "main_tab_clicked2.png");
            this.imageListMainTabMiddle.Images.SetKeyName(3, "main_tab_clicked2.png");
            // 
            // netUpdator
            // 
            this.netUpdator.Interval = 1000;
            this.netUpdator.Tick += new System.EventHandler(this.netUpdator_Tick);
            // 
            // imageListLogout
            // 
            this.imageListLogout.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLogout.ImageStream")));
            this.imageListLogout.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLogout.Images.SetKeyName(0, "logout.png");
            this.imageListLogout.Images.SetKeyName(1, "logoutOver.png");
            this.imageListLogout.Images.SetKeyName(2, "logoutClick.png");
            // 
            // btnUsersTab
            // 
            this.btnUsersTab.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnUsersTab, "btnUsersTab");
            this.btnUsersTab.FlatAppearance.BorderSize = 0;
            this.btnUsersTab.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnUsersTab.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnUsersTab.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnUsersTab.ForeColor = System.Drawing.Color.Black;
            this.btnUsersTab.Name = "btnUsersTab";
            this.btnUsersTab.State = PacketSniffer.UI.TabState.BTN_NORMAL;
            this.btnUsersTab.TabMenuIndex = PacketSniffer.UI.TabMenu.LogManager;
            this.toolTipMain.SetToolTip(this.btnUsersTab, resources.GetString("btnUsersTab.ToolTip"));
            this.btnUsersTab.UseVisualStyleBackColor = false;
            // 
            // btnLogManagerTab
            // 
            this.btnLogManagerTab.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnLogManagerTab, "btnLogManagerTab");
            this.btnLogManagerTab.FlatAppearance.BorderSize = 0;
            this.btnLogManagerTab.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnLogManagerTab.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnLogManagerTab.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnLogManagerTab.ForeColor = System.Drawing.Color.Black;
            this.btnLogManagerTab.ListImage = this.imageListMainTabMiddle;
            this.btnLogManagerTab.Name = "btnLogManagerTab";
            this.btnLogManagerTab.State = PacketSniffer.UI.TabState.BTN_NORMAL;
            this.btnLogManagerTab.TabMenuIndex = PacketSniffer.UI.TabMenu.LogManager;
            this.toolTipMain.SetToolTip(this.btnLogManagerTab, resources.GetString("btnLogManagerTab.ToolTip"));
            this.btnLogManagerTab.UseVisualStyleBackColor = false;
            // 
            // btnReportTab
            // 
            this.btnReportTab.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnReportTab, "btnReportTab");
            this.btnReportTab.FlatAppearance.BorderSize = 0;
            this.btnReportTab.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnReportTab.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnReportTab.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnReportTab.ForeColor = System.Drawing.Color.Black;
            this.btnReportTab.ListImage = this.imageListMainTabMiddle;
            this.btnReportTab.Name = "btnReportTab";
            this.btnReportTab.State = PacketSniffer.UI.TabState.BTN_NORMAL;
            this.btnReportTab.TabMenuIndex = PacketSniffer.UI.TabMenu.CrystalReport;
            this.toolTipMain.SetToolTip(this.btnReportTab, resources.GetString("btnReportTab.ToolTip"));
            this.btnReportTab.UseVisualStyleBackColor = false;
            // 
            // btnLiveMonitorTab
            // 
            this.btnLiveMonitorTab.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnLiveMonitorTab, "btnLiveMonitorTab");
            this.btnLiveMonitorTab.FlatAppearance.BorderSize = 0;
            this.btnLiveMonitorTab.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnLiveMonitorTab.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnLiveMonitorTab.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnLiveMonitorTab.ForeColor = System.Drawing.Color.Black;
            this.btnLiveMonitorTab.Name = "btnLiveMonitorTab";
            this.btnLiveMonitorTab.State = PacketSniffer.UI.TabState.BTN_NORMAL;
            this.btnLiveMonitorTab.TabMenuIndex = PacketSniffer.UI.TabMenu.LiveMonitor;
            this.toolTipMain.SetToolTip(this.btnLiveMonitorTab, resources.GetString("btnLiveMonitorTab.ToolTip"));
            this.btnLiveMonitorTab.UseVisualStyleBackColor = false;
            // 
            // btnLogout
            // 
            resources.ApplyResources(this.btnLogout, "btnLogout");
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnLogout.IsRoundRect = false;
            this.btnLogout.Name = "btnLogout";
            this.toolTipMain.SetToolTip(this.btnLogout, resources.GetString("btnLogout.ToolTip"));
            this.btnLogout.UseVisualStyleBackColor = false;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.btnUsersTab);
            this.Controls.Add(this.btnLogManagerTab);
            this.Controls.Add(this.btnReportTab);
            this.Controls.Add(this.tabPanel);
            this.Controls.Add(this.btnLiveMonitorTab);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnLogout);
            this.Name = "MainForm";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel tabPanel;
        private TabMenuButton btnLiveMonitorTab;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ImageList imageListMainTabLeft;
        private System.Windows.Forms.ImageList imageListMainTabRight;
        private System.Windows.Forms.ImageList imageListMainTabMiddle;
        private System.Windows.Forms.Timer netUpdator;
        private System.Windows.Forms.ImageList imageListLogout;
        private IconButton btnLogout;
        private System.Windows.Forms.ToolTip toolTipMain;
        private TabMenuButton btnReportTab;
        private TabMenuButton btnUsersTab;
        private TabMenuButton btnLogManagerTab;
    }
}