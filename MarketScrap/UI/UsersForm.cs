﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using PacketSniffer.DataBase;
using PacketSniffer.Properties;
 
namespace PacketSniffer.UI
{
    public partial class UsersForm : Form
    {
        private ArrayList userList = new ArrayList();

        public UsersForm()
        {
            InitializeComponent();
        }

        private void OnResize(object sender, EventArgs e)
        {
            tableUserList.Width = Width - btnNewUser.Left * 2;
            tableUserList.Height = Height - btnNewUser.Bottom - 20;
        }

        public void LoadSetting()
        {
            bool isAdmin = (Program.LoginUser.role == (int)EAdminRole.Administrator);
            btnModify.Visible =
                btnDelete.Visible =
                btnNewUser.Visible = isAdmin;

            if (isAdmin)
            {
                Program.MainForm.UserInformationForm.UserInfoBox.Parent = Program.MainForm.UserInformationForm ;
                tableUserList.Visible = true;

                userList = DB01PMUser.LoadPMUsers("");
                if (userList.Count < 1)
                {
                    PMUserEntity entity = new PMUserEntity(SQLDB.SUPER_ADMINISTRATOR, 0, "");
                    DB01PMUser.InsertPMUserEntity(entity);
                }
                
                tableUserList.Rows.Clear();

                foreach (PMUserEntity user in userList)
                {
                    tableUserList.Rows.Add(new object[] 
                    {
                        user.pmUserName,
                        (EAdminRole)user.role,
                        user.LoginTime,
                        user.LogoutTime
                    });

                }
            }
            else
            {
                Program.MainForm.UserInformationForm.UserInfoBox.Parent = this;
                tableUserList.Visible = false;
                
                Program.MainForm.UserInformationForm.UserResult(Program.LoginUser);
            }
        }

        private void btnNewUser_Click(object sender, EventArgs e)
        {
            PMUserEntity user = null;
            if (Program.MainForm.UserInformationForm.UserResult(user))
            {
                user = Program.MainForm.UserInformationForm.resultUser;
                tableUserList.Rows.Add(new object[] 
                    {
                        user.pmUserName,
                        (EAdminRole)user.role,
                        user.LoginTime,
                        user.LogoutTime
                    });

                DB01PMUser.InsertPMUserEntity(user);
            }
        }

        private void btnModifyUser_Click(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = tableUserList.SelectedRows[0].Cells;

            string userName = cells[0].Value.ToString();
            int role = Convert.ToInt16(cells[1].Value);

            PMUserEntity user = DB01PMUser.GetPMUserEntity(userName, role);

            if (Program.MainForm.UserInformationForm.UserResult(user))
            {
                user = Program.MainForm.UserInformationForm.resultUser;
                cells[0].Value = user.pmUserName;
                cells[1].Value = (EAdminRole)user.role;
                cells[2].Value = user.LoginTime;
                cells[3].Value = user.LogoutTime;
                
                DB01PMUser.UpdatePMUserEntity(user);
                Program.MainForm.UserInformationForm.PwdChange = false;
            }
        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = tableUserList.SelectedRows[0].Cells;
            string userName = cells[0].Value.ToString();
            int role = Convert.ToInt16(cells[1].Value);

            if (role == (int)EAdminRole.Administrator)
            {
                if (DB01PMUser.GetPMAdmins() == 1)
                {
                    MessageBox.Show("You can't delete this User.\nThe application must have an Administraotr.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            if (MessageBox.Show("Are you sure to delete this User?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                DB01PMUser.RemovePMUserEntity(userName, role);

                tableUserList.Rows.RemoveAt(tableUserList.Rows[0].Index);
            }
        }

        private void tableUserList_SelectionChanged(object sender, EventArgs e)
        {
            btnDelete.Enabled= (tableUserList.SelectedRows.Count > 0) ;
        }
    }
}
