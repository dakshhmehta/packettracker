﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;
using System.Net.NetworkInformation;
using System.Threading;
 
using System.Collections;
using System.Reflection;
using PacketSniffer.UI;
using PacketSniffer.DataBase;

namespace PacketSniffer.UI
{
    public struct PrevPacket
    {
        public string Address;
        public long ReceivedBytes;
        public long SentBytes;
        public long UploadSpeed;
        public long DownloadSpeed;

        public string ProtocolType;
        public string MyIP;
        public string Description;

        public string DestinationMACAddress;
        public string DestinationName;
        public bool IsWireless;
    }

    public enum Protocol
    {
        Unknown = 0,
        TCP = 6,
        UDP = 17,
        DHCP = -1,
        // TODO make sure these can be here        
        WIRELESS = 18,
        ETHERNET = 19
    };

   
    public partial class MainForm : Form
    {
        #region Class Variable
         
        private TabMenu mCurTabPage;
        public TabMenu CurTabPage
        {
            get { return mCurTabPage; }
            set
            {
                TabPanelShow(mCurTabPage, false);
                mCurTabPage = value;
                TabPanelShow(mCurTabPage, true);
            }
        }

        public UserInfoForm UserInformationForm
        {
            get;
            set;
        }
        public MonitorForm MonitorForm
        {
            get;
            set;
        }
        public ReportForm ReportsForm
        {
            get;
            set;
        }

        public GraphForm LogManagerForm
        {
            get;
            set;
        }

        public UsersForm UserManagerForm
        {
            get;
            set;
        }

        private bool _ServerLogged;
        public bool ServerLogged
        {
            get { return _ServerLogged; }
            set { _ServerLogged = value; }
        }
        //private IconButton btnLogout;

        #endregion

        #region Class Standard Function
        public MainForm()
        {
            InitializeComponent();

            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            ////////// btnLogout ////////////



            //////////////////////////////////


            #region SetControl

            this.btnLiveMonitorTab.TabMenuIndex = TabMenu.LiveMonitor;
            this.btnLogManagerTab.TabMenuIndex = TabMenu.LogManager;
            this.btnUsersTab.TabMenuIndex = TabMenu.Users;
            
            this.btnLogout.ListImage = imageListLogout;

            this.btnLiveMonitorTab.ListImage = imageListMainTabLeft;
            this.btnReportTab.ListImage = imageListMainTabMiddle;
            this.btnLogManagerTab.ListImage = imageListMainTabMiddle;
            this.btnUsersTab.ListImage = imageListMainTabRight;

            this.MinimumSize = new Size(1126, 725);

            Load += new EventHandler(this.OnFormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.Paint += new PaintEventHandler(OnPaint);
            Shown += new EventHandler(Form_Shown);
            Resize += new EventHandler(OnResize);

            this.btnLogout.Click += new EventHandler(btnLogout_Click);

            foreach (Control control in Controls)
                if (control.GetType() == typeof(TabMenuButton))
                    control.Click += new EventHandler(TabMenuClick);


            UserInformationForm = new UserInfoForm();
            
            #endregion

            MoveControls();

        }

        void btnLogout_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }



        private void MoveControls()
        {
            int width = this.Width;
            int heght = this.Height;

            int TabWidth = btnLiveMonitorTab.Width;
            int top = 79;

            int ButtonTOP = 20 + btnLogout.Height / 2;


            this.btnLogout.Location = new Point(Width - btnLogout.Width - 30, ButtonTOP - btnLogout.Height / 2);
            this.btnLiveMonitorTab.Location = new Point(27, top);
            this.btnReportTab.Location = new Point(25 + TabWidth, top);
            this.btnLogManagerTab.Location = new Point(23 + TabWidth * 2, top);
            this.btnUsersTab.Location = new Point(21 + TabWidth * 3, top);
        }
        void OnPaint(object sender, PaintEventArgs e)
        {
            Image image = this.BackgroundImage;
            e.Graphics.DrawImage(image, new Rectangle(0, 0, this.Width, image.Height), new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
        }

        private void OnResize(object sender, EventArgs e)
        {
            int width = this.Width;
            int heght = this.Height;

            tabPanel.Size = new System.Drawing.Size(width - 20, heght - 160);
            Invalidate();
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            {
                btnLiveMonitorTab.State = TabState.BTN_SELECT;
                CurTabPage = TabMenu.LiveMonitor;
                LogManagerForm = new GraphForm();
            }
        }

        public void ShowView(Type type, bool show)
        {
            if (type == typeof(MonitorForm))
            {
                if (MonitorForm == null || MonitorForm.IsDisposed)
                {
                    MonitorForm = new MonitorForm();
                }

                if (show)
                {
                    netUpdator.Start();
                    MonitorForm.TopLevel = false;
                    MonitorForm.Dock = DockStyle.Fill;
                    MonitorForm.Parent = tabPanel;

                    MonitorForm.Show();
                }
                else
                {
                    MonitorForm.Hide();
                }
            }

            else if (type == typeof(ReportForm))
            {
                if (ReportsForm == null || ReportsForm.IsDisposed)
                {
                    ReportsForm = new ReportForm();
                }

                if (show)
                {
                    ReportsForm.TopLevel = false;
                    ReportsForm.Dock = DockStyle.Fill;
                    ReportsForm.Parent = tabPanel;

                    ReportsForm.Show();
                }
                else
                {
                    ReportsForm.Hide();
                }
            }

            else if (type == typeof(GraphForm))
            {
                if (LogManagerForm == null || LogManagerForm.IsDisposed)
                {
                    LogManagerForm = new GraphForm();
                }
                if (show)
                {
                    netUpdator.Start();
                    LogManagerForm.TopLevel = false;
                    LogManagerForm.Dock = DockStyle.Fill;
                    LogManagerForm.Parent = tabPanel;

                    LogManagerForm.Show();
                }
                else
                {
                    LogManagerForm.Hide();
                }
            }
            else if (type == typeof(UsersForm))
            {
                if (UserManagerForm == null || UserManagerForm.IsDisposed)
                {
                    UserManagerForm = new UsersForm();
                }
                if (show)
                {
                    netUpdator.Stop();
                    UserManagerForm.TopLevel = false;
                    UserManagerForm.Dock = DockStyle.Fill;
                    UserManagerForm.Parent = tabPanel;
                    UserManagerForm.LoadSetting();

                    UserManagerForm.Show();
                }
                else
                {
                    UserManagerForm.Hide();
                }
            }
        }

        private void Form_Shown(object sender, EventArgs e)
        {

        }

        private void OnFormClosing(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }


        #endregion

        /// <summary>
        /// Tab Management
        /// </summary>

        #region Tab Management

        private TabMenuButton GetTabMenuButton(TabMenu tab)
        {
            TabMenuButton tabmenu = new TabMenuButton();
            switch (tab)
            {
                case TabMenu.LiveMonitor:
                    tabmenu = btnLiveMonitorTab;
                    break;
                case TabMenu.LogManager:
                    tabmenu = btnLogManagerTab;
                    break;
            }
            return tabmenu;

        }
        void TabPanelShow(TabMenu tabIndex, bool isSelect)
        {
            TabMenuButton tabmenu = new TabMenuButton();
            switch (tabIndex)
            {
                case TabMenu.LiveMonitor:
                    ShowView(typeof(MonitorForm), isSelect);
                    tabmenu = btnLiveMonitorTab;
                    break;
                case TabMenu.CrystalReport:
                    ShowView(typeof(ReportForm), isSelect);
                    tabmenu = btnReportTab;
                    break;
                case TabMenu.LogManager:
                    ShowView(typeof(GraphForm), isSelect);
                    tabmenu = btnLogManagerTab;
                    break;
                case TabMenu.Users:
                    ShowView(typeof(UsersForm), isSelect);
                    tabmenu = btnUsersTab;
                    break;
            }
            if (tabmenu.State == TabState.BTN_NORMAL && !isSelect) return;
            if (isSelect)
                tabmenu.State = TabState.BTN_SELECT;
            else
                tabmenu.State = TabState.BTN_NORMAL;
        }
        void TabMenuClick(object sender, EventArgs e)
        {
            TabMenuClick(((TabMenuButton)sender).TabMenuIndex);
        }

        delegate void TabMenuClickDelegate(TabMenu tab);
        public void TabMenuClick(TabMenu tab)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TabMenuClickDelegate(TabMenuClick), new object[] { this });
                return;
            }

            CurTabPage = tab;
            switch (mCurTabPage)
            {
                case TabMenu.LiveMonitor:
                    //MonitorForm.LoadSetting();
                    break;
                case TabMenu.Users:
                    break;
                case TabMenu.LogManager:
                    //LogManagerForm.UpdateForm();
                    break;

            }
        }
        #endregion

        /// <summary>
        /// ToolTip Menu
        /// </summary>
        #region ToolTip Menu
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        #endregion


        /// <summary>
        /// Button Event
        /// </summary>
        #region Button Event


        #endregion

        private void MainForm_Shown(object sender, EventArgs e)
        {

        }

        private void netUpdator_Tick(object sender, EventArgs e)
        {
            if (MonitorForm.Visible)
                MonitorForm.LoadSetting();
            if (LogManagerForm.Visible)
                LogManagerForm.UpdateForm();
        }


    }

    public class Rates
    {
        public int time;
        public long ULRate;
        public long DLRate;

        public Rates(int now, long ulRate, long dlRate)
        {
            time = now;
            ULRate = ulRate;
            DLRate = dlRate;
        }
    }
}
