﻿namespace PacketSniffer.UI
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtULRate = new System.Windows.Forms.TextBox();
            this.txtDLRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupRateValue = new System.Windows.Forms.GroupBox();
            this.groupGraph = new System.Windows.Forms.GroupBox();
            this.cbMaxSpeed = new System.Windows.Forms.ComboBox();
            this.panelGraph = new System.Windows.Forms.Panel();
            this.lblULColor = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDLColor = new System.Windows.Forms.Label();
            this.lblUL = new System.Windows.Forms.Label();
            this.lblDL = new System.Windows.Forms.Label();
            this.lblUnit = new System.Windows.Forms.Label();
            this.groupRateValue.SuspendLayout();
            this.groupGraph.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Upload Rate :";
            // 
            // txtULRate
            // 
            this.txtULRate.Location = new System.Drawing.Point(150, 42);
            this.txtULRate.Name = "txtULRate";
            this.txtULRate.ReadOnly = true;
            this.txtULRate.Size = new System.Drawing.Size(194, 23);
            this.txtULRate.TabIndex = 1;
            // 
            // txtDLRate
            // 
            this.txtDLRate.Location = new System.Drawing.Point(522, 42);
            this.txtDLRate.Name = "txtDLRate";
            this.txtDLRate.ReadOnly = true;
            this.txtDLRate.Size = new System.Drawing.Size(194, 23);
            this.txtDLRate.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(389, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Download Rate :";
            // 
            // groupRateValue
            // 
            this.groupRateValue.Controls.Add(this.txtDLRate);
            this.groupRateValue.Controls.Add(this.label1);
            this.groupRateValue.Controls.Add(this.txtULRate);
            this.groupRateValue.Controls.Add(this.label2);
            this.groupRateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupRateValue.Location = new System.Drawing.Point(32, 24);
            this.groupRateValue.Name = "groupRateValue";
            this.groupRateValue.Size = new System.Drawing.Size(1019, 100);
            this.groupRateValue.TabIndex = 3;
            this.groupRateValue.TabStop = false;
            this.groupRateValue.Text = "Current Rate Value";
            // 
            // groupGraph
            // 
            this.groupGraph.Controls.Add(this.lblUnit);
            this.groupGraph.Controls.Add(this.cbMaxSpeed);
            this.groupGraph.Controls.Add(this.panelGraph);
            this.groupGraph.Controls.Add(this.lblULColor);
            this.groupGraph.Controls.Add(this.label4);
            this.groupGraph.Controls.Add(this.label3);
            this.groupGraph.Controls.Add(this.lblDLColor);
            this.groupGraph.Controls.Add(this.lblUL);
            this.groupGraph.Controls.Add(this.lblDL);
            this.groupGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupGraph.Location = new System.Drawing.Point(32, 158);
            this.groupGraph.Name = "groupGraph";
            this.groupGraph.Size = new System.Drawing.Size(1019, 328);
            this.groupGraph.TabIndex = 3;
            this.groupGraph.TabStop = false;
            this.groupGraph.Text = "Current Rate Graph";
            // 
            // cbMaxSpeed
            // 
            this.cbMaxSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaxSpeed.FormattingEnabled = true;
            this.cbMaxSpeed.Items.AddRange(new object[] {
            "1",
            "3",
            "10",
            "20",
            "50",
            "100",
            "200",
            "500",
            "1024",
            "512000",
            "102400"});
            this.cbMaxSpeed.Location = new System.Drawing.Point(866, 48);
            this.cbMaxSpeed.Name = "cbMaxSpeed";
            this.cbMaxSpeed.Size = new System.Drawing.Size(82, 24);
            this.cbMaxSpeed.TabIndex = 1;
            this.cbMaxSpeed.SelectedIndexChanged += new System.EventHandler(this.cbMaxSpeed_SelectedIndexChanged);
            // 
            // panelGraph
            // 
            this.panelGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGraph.Location = new System.Drawing.Point(37, 89);
            this.panelGraph.Name = "panelGraph";
            this.panelGraph.Size = new System.Drawing.Size(956, 214);
            this.panelGraph.TabIndex = 0;
            this.panelGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraph_Paint);
            // 
            // lblULColor
            // 
            this.lblULColor.BackColor = System.Drawing.Color.Blue;
            this.lblULColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblULColor.Location = new System.Drawing.Point(150, 51);
            this.lblULColor.Name = "lblULColor";
            this.lblULColor.Size = new System.Drawing.Size(194, 17);
            this.lblULColor.TabIndex = 0;
            this.lblULColor.Text = " ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(954, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "KB / s";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(751, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Limited Rate :";
            // 
            // lblDLColor
            // 
            this.lblDLColor.BackColor = System.Drawing.Color.Lime;
            this.lblDLColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDLColor.Location = new System.Drawing.Point(522, 51);
            this.lblDLColor.Name = "lblDLColor";
            this.lblDLColor.Size = new System.Drawing.Size(194, 17);
            this.lblDLColor.TabIndex = 0;
            this.lblDLColor.Text = " ";
            // 
            // lblUL
            // 
            this.lblUL.AutoSize = true;
            this.lblUL.Location = new System.Drawing.Point(39, 51);
            this.lblUL.Name = "lblUL";
            this.lblUL.Size = new System.Drawing.Size(108, 17);
            this.lblUL.TabIndex = 0;
            this.lblUL.Text = "Upload Rate :";
            // 
            // lblDL
            // 
            this.lblDL.AutoSize = true;
            this.lblDL.Location = new System.Drawing.Point(389, 51);
            this.lblDL.Name = "lblDL";
            this.lblDL.Size = new System.Drawing.Size(127, 17);
            this.lblDL.TabIndex = 0;
            this.lblDL.Text = "Download Rate :";
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnit.Location = new System.Drawing.Point(27, 72);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(21, 13);
            this.lblUnit.TabIndex = 2;
            this.lblUnit.Text = "KB";
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 560);
            this.ControlBox = false;
            this.Controls.Add(this.groupGraph);
            this.Controls.Add(this.groupRateValue);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GraphForm";
            this.groupRateValue.ResumeLayout(false);
            this.groupRateValue.PerformLayout();
            this.groupGraph.ResumeLayout(false);
            this.groupGraph.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtULRate;
        private System.Windows.Forms.TextBox txtDLRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupRateValue;
        private System.Windows.Forms.GroupBox groupGraph;
        private System.Windows.Forms.Panel panelGraph;
        private System.Windows.Forms.Label lblULColor;
        private System.Windows.Forms.Label lblDLColor;
        private System.Windows.Forms.Label lblUL;
        private System.Windows.Forms.Label lblDL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbMaxSpeed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblUnit;

    }
}