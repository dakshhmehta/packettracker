﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using PacketSniffer.DataBase;

namespace PacketSniffer.UI
{
    public partial class LoginForm : Form
    {
        private ArrayList m_admins = new ArrayList();
        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginSuccess(PMUserEntity adminEntity)
        {
            Properties.Settings.Default.LoginPassword = txtPassword.Text;
            Properties.Settings.Default.LoginUsername = adminEntity.pmUserName;
            Properties.Settings.Default.Role = adminEntity.role;
            Properties.Settings.Default.Save();
            this.DialogResult = DialogResult.OK;
            this.Close();

            adminEntity.password = txtPassword.Text;
            adminEntity.LoginTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            adminEntity.LogoutTime = "-";
            Program.LoginUser = adminEntity;

            DB01PMUser.UpdatePMUserEntity(adminEntity);
        }

        private void btnLoginUser_Click(object sender, EventArgs e)
        {
            string sn = this.cbPMUsers.Text;
            PMUserEntity m_loginAdmin = null;
            
            if (string.IsNullOrEmpty(sn))
            {
                lblMsgState.ForeColor = Color.Red;
                lblMsgState.Text = "Please input User Name!";
                this.cbPMUsers.Focus();
                return;
            }

            m_loginAdmin = (PMUserEntity)m_admins[cbPMUsers.SelectedIndex];

            if (m_loginAdmin.password == txtPassword.Text.GetHashCode().ToString())
            {
                LoginSuccess(m_loginAdmin);
            }
            else
            {
                lblMsgState.ForeColor = Color.Red;
                lblMsgState.Text = "Password is incorrect!";
                this.txtPassword.Focus();
                return;
            }             
        }
         
        private void InitCmbAdmin()
        {
            cbPMUsers.Items.Clear(); int k = 0, prev = 0;
            foreach (PMUserEntity admin in m_admins)
            {
                cbPMUsers.Items.Add(string.Format("{0} ({1})", admin.pmUserName, (EAdminRole)admin.role));
                if (admin.pmUserName == Properties.Settings.Default.LoginUsername) prev = k;
                k++;
            }
            if (cbPMUsers.Items.Count > 0)
                cbPMUsers.SelectedIndex = prev;
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            m_admins = DB01PMUser.LoadPMUsers("");
            if (m_admins.Count < 1)
            {
                PMUserEntity entity = new PMUserEntity(SQLDB.SUPER_ADMINISTRATOR, 0,   "" );
                DB01PMUser.InsertPMUserEntity(entity);
            }
            InitCmbAdmin();

            chkRememberPwd.Checked = Properties.Settings.Default.RememberPassword;
            if ( chkRememberPwd.Checked  )
            {
                this.cbPMUsers.Text = Properties.Settings.Default.LoginUsername;
                this.txtPassword.Text = Properties.Settings.Default.LoginPassword;
            }
        }

        private void cbPMUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPassword.Text = "";
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (new RegisterForm().ShowDialog() == DialogResult.OK)
            {
                // new user validated, do the login
                this.DialogResult = DialogResult.OK;
                this.Close();                
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}
