﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Management;
using System.ServiceProcess;
using Microsoft.Win32;

using PacketSniffer.UI;
using PacketSniffer.Headers;
using PacketSniffer.DataBase;
using System.Runtime.InteropServices;

namespace PacketSniffer.UI
{

    public partial class MonitorForm : Form
    {
        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(int destIp, int srcIP, byte[] macAddr, ref uint physicalAddrLen);
        public static string[] HexConversionLib = Enumerable.Range(0, 256).Select(v => v.ToString("X2")).ToArray();

        const int LEFT_POS = 20;
        public const int MAXBUFFER = 8192;

        private byte[] byteData = new byte[MAXBUFFER];
        private byte[] byteRecData = new byte[MAXBUFFER];
        private Socket[] mainSocket;                          //The socket which captures all incoming packets
        private Socket[] recSocket;                          //The socket which captures all incoming packets
        private List<IPAddress> hostIP;
        private Dictionary<string, string> machineNames;
        private Dictionary<string, string> maccache; 
        private static object lockObject = new object();

        private NetworkInterface[] nicArray;
        static List<PrevPacket> prevPackets;
        public static List<ReportEntity> reportList;
        private DataGridViewCheckBoxColumn WirelessColumn;
        private DataGridViewTextBoxColumn DestinationMACAddressColumn;
        private DataGridViewTextBoxColumn DestinationNameColumn;
        private DataGridViewTextBoxColumn DescriptionColumn;
        private DataGridViewTextBoxColumn emailColumn;
        private DataGridViewTextBoxColumn nameColumn;
        private DataGridViewTextBoxColumn IDColumn;
        private DataGridViewTextBoxColumn NetworkType;
        private DataGridViewTextBoxColumn NetAdapter;
        private DataGridViewTextBoxColumn NoColumn;
        List<NICHelper> nicDatas;

        public MonitorForm()
        {
            int i = 0;
            InitializeComponent();
            nicArray = NetworkInterface.GetAllNetworkInterfaces();
            prevPackets = new List<PrevPacket>();
            reportList = new List<ReportEntity>();
            nicDatas = new List<NICHelper>();
            machineNames = new Dictionary<string, string>();
            maccache = new Dictionary<string, string>();

            for (i = 0; i < nicArray.Length; i++)
            {
                UnicastIPAddressInformationCollection ipInfo = nicArray[i].GetIPProperties().UnicastAddresses;
                if (!ipInfo.Any(o => o.Address.AddressFamily == AddressFamily.InterNetwork))
                    continue;

                NICHelper nic = new NICHelper
                {
                    Name = nicArray[i].Name,
                    Description = nicArray[i].Description,
                    Type = nicArray[i].NetworkInterfaceType,
                    MACAddress = ParseMACAddress(nicArray[i].GetPhysicalAddress().GetAddressBytes()),
                    IPAddresses = ipInfo.Where(o => o.Address.AddressFamily == AddressFamily.InterNetwork).Select(o => o.Address.ToString()).ToList()
                };

                nicDatas.Add(nic);
#if NOT
                IPv4InterfaceStatistics data = nicArray[i].GetIPv4Statistics();
                tablePacketList.Rows.Add(new object[]{
                        nicArray[i].Name,
                        nicArray[i].NetworkInterfaceType,
                        prevPackets[i].Address,
                        0,
                        0,
                        nicArray[i].Description
                    });
                prevPackets[i].ReceivedBytes = data.BytesReceived;
                prevPackets[i].SentBytes = data.BytesSent;
#endif
            }

            IPHostEntry HostEntry = Dns.GetHostEntry((Dns.GetHostName()));
            int len = HostEntry.AddressList.Length;
            hostIP = new List<IPAddress>();

            if (len == 0)
            {
                len = 1;
                hostIP.Add(IPAddress.Parse(nicDatas[0].IPAddresses.First()));
            }
            else
            {
                for (i = 0; i < len; i++)
                {
                    try
                    {
                        if (HostEntry.AddressList[i].GetAddressBytes().Any(o=> o > 0) 
                            && HostEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                        {
                            hostIP.Add(HostEntry.AddressList[i]);
                            //string strIP = HostEntry.AddressList[i].ToString();
                            //hostIP.Add(strIP);
                        }
                    }
                    catch
                    { }

                }
            }
            mainSocket = new Socket[hostIP.Count];
            recSocket = new Socket[hostIP.Count];

            for (i = 0; i < hostIP.Count; i++)
            {
                //For sniffing the socket to capture the packets has to be a raw socket, with the
                //address family being of type internetwork, and protocol being IP
                mainSocket[i] = new Socket(AddressFamily.InterNetwork,
                    SocketType.Raw, ProtocolType.IP);

                //Bind the socket to the selected IP address                
                mainSocket[i].Bind(new IPEndPoint(hostIP[i], 0));


                //Set the socket  options
                mainSocket[i].SetSocketOption(SocketOptionLevel.IP,            //Applies only to IP packets
                                           SocketOptionName.HeaderIncluded, //Set the include the header
                                           true);                           //option to true

                byte[] byTrue = new byte[4] { 1, 0, 0, 0 };
                byte[] byOut = new byte[4] { 1, 0, 0, 0 }; //Capture outgoing packets

                //Socket.IOControl is analogous to the WSAIoctl method of Winsock 2
                mainSocket[i].IOControl(IOControlCode.ReceiveAll,              //Equivalent to SIO_RCVALL constant
                                                                               //of Winsock 2
                                     byTrue,
                                     byOut);

                //Start receiving the packets asynchronously
                mainSocket[i].BeginReceive(byteData, 0, byteData.Length, SocketFlags.None,
                    new AsyncCallback(OnReceive), i);

                //For sniffing the socket to capture the packets has to be a raw socket, with the
                //address family being of type internetwork, and protocol being IP
                recSocket[i] = new Socket(AddressFamily.InterNetwork,
                    SocketType.Raw, ProtocolType.IP);

                //Bind the socket to the selected IP address
                recSocket[i].Bind(new IPEndPoint(hostIP[i], 0));

                //Set the socket  options
                recSocket[i].SetSocketOption(SocketOptionLevel.IP,            //Applies only to IP packets
                                           SocketOptionName.HeaderIncluded, //Set the include the header
                                           true);                           //option to true
                
                byTrue = new byte[4] { 1, 0, 0, 0 };
                byOut = new byte[4] { 0, 0, 0, 0 }; //Capture ingoing packets

                //Socket.IOControl is analogous to the WSAIoctl method of Winsock 2
                recSocket[i].IOControl(IOControlCode.DataToRead,              //Equivalent to SIO_RCVALL constant
                                                                              //of Winsock 2
                                     byTrue,
                                     byOut);

                //Start receiving the packets asynchronously
                recSocket[i].BeginReceive(byteRecData, 0, byteRecData.Length, SocketFlags.None,
                    new AsyncCallback(OnSend), i);
            }
        }

        private string ParseMACAddress(IEnumerable<byte> bytes)
        {
            StringBuilder sb = new StringBuilder(bytes.Count() * 2);
            foreach (byte b in bytes)
            {
                sb.Append(HexConversionLib[b]);
            }

            return sb.ToString();
        }

        #region Windows Form Designer generated code

        private DataGridView tablePacketList;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitorForm));
            this.tablePacketList = new System.Windows.Forms.DataGridView();
            this.NoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAdapter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WirelessColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DestinationMACAddressColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DestinationNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetworkType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tablePacketList)).BeginInit();
            this.SuspendLayout();
            // 
            // tablePacketList
            // 
            this.tablePacketList.AllowUserToAddRows = false;
            this.tablePacketList.AllowUserToDeleteRows = false;
            this.tablePacketList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablePacketList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NoColumn,
            this.NetAdapter,
            this.WirelessColumn,
            this.DestinationMACAddressColumn,
            this.DestinationNameColumn,
            this.NetworkType,
            this.IDColumn,
            this.nameColumn,
            this.emailColumn,
            this.DescriptionColumn});
            resources.ApplyResources(this.tablePacketList, "tablePacketList");
            this.tablePacketList.Name = "tablePacketList";
            this.tablePacketList.ReadOnly = true;
            this.tablePacketList.RowHeadersVisible = false;
            // 
            // NoColumn
            // 
            this.NoColumn.FillWeight = 50F;
            resources.ApplyResources(this.NoColumn, "NoColumn");
            this.NoColumn.Name = "NoColumn";
            this.NoColumn.ReadOnly = true;
            // 
            // NetAdapter
            // 
            this.NetAdapter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NetAdapter.FillWeight = 200F;
            resources.ApplyResources(this.NetAdapter, "NetAdapter");
            this.NetAdapter.Name = "NetAdapter";
            this.NetAdapter.ReadOnly = true;
            // 
            // WirelessColumn
            // 
            resources.ApplyResources(this.WirelessColumn, "WirelessColumn");
            this.WirelessColumn.Name = "WirelessColumn";
            this.WirelessColumn.ReadOnly = true;
            // 
            // DestinationMACAddressColumn
            // 
            resources.ApplyResources(this.DestinationMACAddressColumn, "DestinationMACAddressColumn");
            this.DestinationMACAddressColumn.Name = "DestinationMACAddressColumn";
            this.DestinationMACAddressColumn.ReadOnly = true;
            // 
            // DestinationNameColumn
            // 
            resources.ApplyResources(this.DestinationNameColumn, "DestinationNameColumn");
            this.DestinationNameColumn.Name = "DestinationNameColumn";
            this.DestinationNameColumn.ReadOnly = true;
            // 
            // NetworkType
            // 
            this.NetworkType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NetworkType.FillWeight = 200F;
            resources.ApplyResources(this.NetworkType, "NetworkType");
            this.NetworkType.Name = "NetworkType";
            this.NetworkType.ReadOnly = true;
            // 
            // IDColumn
            // 
            this.IDColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IDColumn.FillWeight = 150F;
            resources.ApplyResources(this.IDColumn, "IDColumn");
            this.IDColumn.Name = "IDColumn";
            this.IDColumn.ReadOnly = true;
            // 
            // nameColumn
            // 
            this.nameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            resources.ApplyResources(this.nameColumn, "nameColumn");
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.ReadOnly = true;
            // 
            // emailColumn
            // 
            this.emailColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            resources.ApplyResources(this.emailColumn, "emailColumn");
            this.emailColumn.Name = "emailColumn";
            this.emailColumn.ReadOnly = true;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DescriptionColumn.FillWeight = 200F;
            resources.ApplyResources(this.DescriptionColumn, "DescriptionColumn");
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.ReadOnly = true;
            // 
            // MonitorForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.tablePacketList);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MonitorForm";
            this.Resize += new System.EventHandler(this.OnResize);
            ((System.ComponentModel.ISupportInitialize)(this.tablePacketList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public void LoadSetting()
        {
            int i = 0;
            try
            {
#if NOT
                for (  i = 0; i < nicArray.Length; i++)
                {
                    IPv4InterfaceStatistics data = nicArray[i].GetIPv4Statistics();
                    DataGridViewCellCollection cells =  tablePacketList.Rows[i].Cells;
                    cells[0].Value =    nicArray[i].Name;
                    cells[1].Value = nicArray[i].NetworkInterfaceType;
                    cells[2].Value = prevPackets[i].Address;
                    speed = data.BytesReceived - prevPackets[i].ReceivedBytes;
                    cells[3].Value = GetSpeed(speed);
                    speed = data.BytesSent - prevPackets[i].SentBytes;
                    cells[4].Value = GetSpeed(speed);
                    cells[5].Value = nicArray[i].Description;
                    
                    prevPackets[i].ReceivedBytes = data.BytesReceived;
                    prevPackets[i].SentBytes = data.BytesSent;

                }
                
                Process[] processes = Process.GetProcesses();
                List<Process> list = new List<Process>();
                foreach (Process sv in processes)
                {
                    if (!string.IsNullOrEmpty(sv.MainWindowTitle))
                    {
                        list.Add(sv);
                    }
                }
#else


                for (i = 0; i < tablePacketList.RowCount; i++)
                {
                    PrevPacket item = prevPackets[i];
                    DataGridViewCellCollection cells = tablePacketList.Rows[i].Cells;

                    cells[0].Value = i + 1;
                    cells[1].Value = item.Address;
                    cells[2].Value = item.IsWireless;
                    cells[3].Value = item.DestinationMACAddress;
                    cells[4].Value = item.DestinationName;
                    cells[5].Value = item.ProtocolType;
                    cells[6].Value = item.MyIP;
                    cells[7].Value = GetSpeed(item.DownloadSpeed);
                    cells[8].Value = GetSpeed(item.UploadSpeed);
                    cells[9].Value = item.Description;
                    item.DownloadSpeed = 0;
                    item.UploadSpeed = 0;
                }
                for (i = tablePacketList.RowCount; i < prevPackets.Count; i++)
                {
                    PrevPacket item = prevPackets[i];

                    tablePacketList.Rows.Add(new object[]{
                        tablePacketList.RowCount + 1,
                        item.Address,
                        item.IsWireless,
                        item.DestinationMACAddress,
                        item.DestinationName,
                        item.ProtocolType,
                        item.MyIP,
                        GetSpeed(item.DownloadSpeed),
                        GetSpeed(item.UploadSpeed),
                        item.Description
                    });
                    item.DownloadSpeed = 0;
                    item.UploadSpeed = 0;
                }
#endif
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }

        public static string GetSpeed(long speed)
        {
            double res = speed;
            if (speed < 1024)
                return speed.ToString() + " byte/s";
            else
            {
                res = speed / 1024.0;
                return res.ToString("#.###") + " KB/s";
            }
        }

        private void OnResize(object sender, EventArgs e)
        {
            tablePacketList.Bounds = new Rectangle(LEFT_POS, LEFT_POS, Width - LEFT_POS * 2, Height - LEFT_POS * 2);
        }

        private bool receiveLock = true;

        private void OnReceive(IAsyncResult ar)
        {
            //lock (receiveLock)
            {
                try
                {
                    int i = 0;
                    if (ar.AsyncState != null)
                        i = (int)ar.AsyncState;

                    int nReceived = mainSocket[i].EndReceive(ar);

                    //Analyze the bytes received...

                    ParseData(byteData, nReceived);

                    byteData = new byte[MAXBUFFER];

                    //Another call to BeginReceive so that we continue to receive the incoming
                    //packets
                    mainSocket[i].BeginReceive(byteData, 0, byteData.Length, SocketFlags.None,
                        new AsyncCallback(OnReceive), i);
                }
                catch (ObjectDisposedException)
                {
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            {
                try
                {
                    int i = 0;
                    if (ar.AsyncState != null)
                        i = (int)ar.AsyncState;
                    int nReceived = recSocket[i].EndReceive(ar);

                    //Analyze the bytes received...

                    ParseData(byteRecData, nReceived, false);

                    byteRecData = new byte[MAXBUFFER];

                    //Another call to BeginReceive so that we continue to receive the incoming
                    //packets
                    recSocket[i].BeginReceive(byteRecData, 0, byteRecData.Length, SocketFlags.None,
                        new AsyncCallback(OnSend), i);
                }
                catch (ObjectDisposedException)
                {
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void ParseData(byte[] bytedata, int nReceived, bool send = true)
        {
            string myIp = "";
            string outIP = "";
            int i = 0;
            int download = -1;

            //Since all protocol packets are encapsulated in the IP datagram
            //so we start by parsing the IP header and see what protocol data
            //is being carried by it
            IPHeader ipHeader = new IPHeader(bytedata, nReceived);
            FrameHeader frame = null;

            foreach (NICHelper nic in nicDatas)
            {
                foreach (string ip in nic.IPAddresses)
                {
                    if (ip == ipHeader.SourceAddress.ToString())
                    {
                        download = 2;
                        myIp = ipHeader.SourceAddress.ToString();
                        outIP = ipHeader.DestinationAddress.ToString();
                        frame = ParseFrame(nic, ipHeader);
                        break;
                    }
                    else if (ip == ipHeader.DestinationAddress.ToString())
                    {
                        download = 1;
                        ipHeader.IsDownload = true;
                        outIP = ipHeader.SourceAddress.ToString();
                        myIp = ipHeader.DestinationAddress.ToString();
                        frame = ParseFrame(nic, ipHeader);
                        break;
                    }
                }
            }

            for (i = 0; i < reportList.Count; i++)
            {
                if (reportList[i].SourceIP == ipHeader.SourceAddress.ToString() && reportList[i].DestinationIP == ipHeader.DestinationAddress.ToString())
                {
                    ReportEntity item = reportList[i];
                    if (download == 1)
                        item.UploadData += nReceived;
                    else
                        item.DownloadData += nReceived;
                    item.Type = ipHeader.ProtocolType;
                    item.TotalPackets++;
                    if (download < 0)
                        item.ManagementPackets++;
                    else if (nReceived == Convert.ToInt32(ipHeader.HeaderLength))
                        item.ControlPackets++;
                    else if (nReceived == Convert.ToInt32(ipHeader.MessageLength) || outIP == "0.0.0.0")
                        item.ManagementPackets++;
                    else
                        item.DataPackets++;
                    break;
                }
            }

            if (i == reportList.Count)
            {
                if (receiveLock)
                {
                    receiveLock = false;
                    ReportEntity item = new ReportEntity();
                    item.SerialNo = reportList.Count + 1;
                    item.SourceIP = ipHeader.SourceAddress.ToString();
                    item.DestinationIP = ipHeader.DestinationAddress.ToString();
                    item.Date = DateTime.Now.ToString("yyyy/MM/dd");
                    item.Time = DateTime.Now.ToString("HH:mm:ss");

                    if (frame != null)
                    {
                        item.SourceMACAddress = frame.SourceMACAddress;
                        item.DestinationMACAddress = frame.DestinationMACAddress;
                        item.IsWireless = frame.Protocol == Protocol.WIRELESS;
                    }

                    item.DestinationName = GetDestinationName(ipHeader.DestinationAddress);

                    if (download == 1)
                        item.UploadData += nReceived;
                    else
                        item.DownloadData += nReceived;
                    item.Type = ipHeader.ProtocolType;
                    item.TotalPackets++;
                    if (download < 0)
                        item.ControlPackets++;
                    else if (nReceived == Convert.ToInt32(ipHeader.HeaderLength))
                        item.ManagementPackets++;
                    else if (nReceived == Convert.ToInt32(ipHeader.MessageLength) || outIP == "0.0.0.0")
                        item.OtherPackets++;
                    else
                        item.DataPackets++;

                    reportList.Add(item);
                    receiveLock = true;
                }
            }
            if (download < 0)
            {
                download = 1;
                outIP = ipHeader.SourceAddress.ToString();
                myIp = ipHeader.DestinationAddress.ToString();
            }
            while (!receiveLock) ;
            for (i = 0; i < prevPackets.Count; i++)
            {
                if (prevPackets[i].Address == outIP)
                {
                    PrevPacket item = prevPackets[i];
                    if (download == 1)
                        item.UploadSpeed += nReceived;
                    else
                        item.DownloadSpeed += nReceived;
                    break;
                }
            }

            if (i == prevPackets.Count && outIP != "0.0.0.0")
            {
                if (receiveLock)
                {
                    receiveLock = false;
                    PrevPacket item = new PrevPacket();
                    item.Address = outIP;
                    item.DestinationName = GetDestinationName(item.Address);
                    item.DestinationMACAddress = GetMACAddress(item.Address);

                    if (download == 1)
                    {
                        item.UploadSpeed = nReceived;
                        item.DestinationName = GetDestinationName(ipHeader.SourceAddress);
                        if (frame != null)
                        {
                            item.DestinationMACAddress = frame.SourceMACAddress == frame.DestinationMACAddress ? "" : frame.SourceMACAddress;
                            item.IsWireless = frame.Protocol == Protocol.WIRELESS;
                        }
                    }
                    else
                    { 
                        item.DownloadSpeed = nReceived;
                        item.DestinationName = GetDestinationName(ipHeader.DestinationAddress);
                        if (frame != null)
                        {
                            //item.DestinationMACAddress = frame.DestinationMACAddress;
                            item.IsWireless = frame.Protocol == Protocol.WIRELESS;
                        }
                    }
                    item.ProtocolType = ipHeader.ProtocolType.ToString();
                    item.MyIP = myIp;
                    item.Description = ipHeader.Version;

                    prevPackets.Add(item);
                    receiveLock = true;
                }
            }

        }

        private string GetDestinationName(string ipAddress)
        {
            if (!machineNames.ContainsKey(ipAddress))
            {
                lock (lockObject)
                {
                    if (!machineNames.ContainsKey(ipAddress))
                    {
                        try
                        {
                            machineNames[ipAddress] = GetDestinationName(IPAddress.Parse(ipAddress));
                        }
                        catch
                        {
                            machineNames[ipAddress] = "";
                        }
                    }
                }
            }

            return machineNames[ipAddress];
        }

        private string GetDestinationName(IPAddress iPAddress)
        {
            if (iPAddress == null)
                return null;

            string key = iPAddress.ToString();

            if (!machineNames.ContainsKey(key))
            {
                lock (lockObject)
                {
                    if (!machineNames.ContainsKey(key))
                    {
                        string resolvedName = String.Empty;
                        try
                        {
                            IPHostEntry host = Dns.GetHostEntry(iPAddress);
                           resolvedName = (host ?? new IPHostEntry()).HostName;
                        }
                        catch (SocketException)
                        {
                               // Name can't resolved, just ignore.
                        }

                        machineNames[iPAddress.ToString()] = resolvedName;
                    }
                }                
            }

            return machineNames[key];            
        }

        private string GetMACAddress(IPAddress ipAddress)
        {
            byte[] macAddr = new byte[6];
            uint macAddrLen = 6;

            if (SendARP(BitConverter.ToInt32(ipAddress.GetAddressBytes(), 0), 0, macAddr, ref macAddrLen) != 0)
                throw new InvalidOperationException("SendARP failed.");
       
            return ParseMACAddress(macAddr);
        }

        private string GetMACAddress(string ipAddress)
        {
            if (!maccache.ContainsKey(ipAddress))
            {
                lock (lockObject)
                {
                    if (!maccache.ContainsKey(ipAddress))
                    {
                        try
                        {
                            maccache[ipAddress] = GetMACAddress(IPAddress.Parse(ipAddress));
                        }
                        catch
                        {
                            maccache[ipAddress] = "";
                        }
                    }
                }
            }

            return maccache[ipAddress];
        }

        private FrameHeader ParseFrame(NICHelper nic, IPHeader ipHeader)
        {
            FrameHeader retVal = new FrameHeader
            {
                SourceMACAddress =  ipHeader.IsDownload ? GetMACAddress(ipHeader.SourceAddress) : nic.MACAddress,                
                DestinationMACAddress = ipHeader.IsDownload ? nic.MACAddress : GetMACAddress(ipHeader.DestinationAddress)            
            };

            switch (nic.Type)
            {
                case NetworkInterfaceType.FastEthernetT:
                case NetworkInterfaceType.Ethernet:
                case NetworkInterfaceType.FastEthernetFx:
                case NetworkInterfaceType.GigabitEthernet:
                    retVal.Protocol = Protocol.ETHERNET;
                    break;
                case NetworkInterfaceType.Wireless80211:
                    retVal.Protocol = Protocol.WIRELESS;
                    break;
                default:
                    retVal.Protocol = Protocol.Unknown;
                    break;
            }

            return retVal;
        }      
    }

    public class NICHelper
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public NetworkInterfaceType Type { get; set; }

        public string MACAddress { get; set; }

        public List<string> IPAddresses { get; set; }
    }
}
