﻿namespace PacketSniffer.UI
{
    partial class LoginForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.chkRememberPwd = new System.Windows.Forms.CheckBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblMsgState = new System.Windows.Forms.Label();
            this.cbPMUsers = new System.Windows.Forms.ComboBox();
            this.btnCancel = new PacketSniffer.UI.UserButton();
            this.btnLoginUser = new PacketSniffer.UI.UserButton();
            this.btnRegister = new PacketSniffer.UI.UserButton();
            this.SuspendLayout();
            // 
            // chkRememberPwd
            // 
            resources.ApplyResources(this.chkRememberPwd, "chkRememberPwd");
            this.chkRememberPwd.BackColor = System.Drawing.Color.Transparent;
            this.chkRememberPwd.Checked = true;
            this.chkRememberPwd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRememberPwd.Name = "chkRememberPwd";
            this.chkRememberPwd.UseVisualStyleBackColor = false;
            // 
            // txtPassword
            // 
            resources.ApplyResources(this.txtPassword, "txtPassword");
            this.txtPassword.Name = "txtPassword";
            // 
            // lblUsername
            // 
            this.lblUsername.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblUsername, "lblUsername");
            this.lblUsername.Name = "lblUsername";
            // 
            // lblPassword
            // 
            this.lblPassword.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblPassword, "lblPassword");
            this.lblPassword.Name = "lblPassword";
            // 
            // lblMsgState
            // 
            resources.ApplyResources(this.lblMsgState, "lblMsgState");
            this.lblMsgState.BackColor = System.Drawing.Color.Transparent;
            this.lblMsgState.ForeColor = System.Drawing.Color.Red;
            this.lblMsgState.Name = "lblMsgState";
            // 
            // cbPMUsers
            // 
            this.cbPMUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPMUsers.FormattingEnabled = true;
            this.cbPMUsers.Items.AddRange(new object[] {
            resources.GetString("cbPMUsers.Items"),
            resources.GetString("cbPMUsers.Items1")});
            resources.ApplyResources(this.cbPMUsers, "cbPMUsers");
            this.cbPMUsers.Name = "cbPMUsers";
            this.cbPMUsers.SelectedIndexChanged += new System.EventHandler(this.cbPMUsers_SelectedIndexChanged);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCancel.IsRoundRect = false;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnLoginUser
            // 
            resources.ApplyResources(this.btnLoginUser, "btnLoginUser");
            this.btnLoginUser.BackColor = System.Drawing.Color.Transparent;
            this.btnLoginUser.FlatAppearance.BorderSize = 0;
            this.btnLoginUser.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnLoginUser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnLoginUser.IsRoundRect = false;
            this.btnLoginUser.Name = "btnLoginUser";
            this.btnLoginUser.UseVisualStyleBackColor = false;
            this.btnLoginUser.Click += new System.EventHandler(this.btnLoginUser_Click);
            // 
            // btnRegister
            // 
            resources.ApplyResources(this.btnRegister, "btnRegister");
            this.btnRegister.BackColor = System.Drawing.Color.Transparent;
            this.btnRegister.FlatAppearance.BorderSize = 0;
            this.btnRegister.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnRegister.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnRegister.IsRoundRect = false;
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.UseVisualStyleBackColor = false;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // LoginForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.cbPMUsers);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblMsgState);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.chkRememberPwd);
            this.Controls.Add(this.btnLoginUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.UserButton btnLoginUser;
        private System.Windows.Forms.CheckBox chkRememberPwd;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblMsgState;
        private UserButton btnCancel;
        private System.Windows.Forms.ComboBox cbPMUsers;
        private UserButton btnRegister;
    }
}
