﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
 
namespace PacketSniffer.UI
{
    public enum TabMenu
    { 
        LiveMonitor = 0,
        CrystalReport,
        LogManager,
        Users,    
    }
    public enum TabState
    { 
        BTN_NORMAL = 0,
        BTN_OVER,
        BTN_DOWN ,
        BTN_SELECT 
    }
    public partial class TabMenuButton : Button
    {
        const int STARTPOSX = 27;
        const int STARTPOSY=80;
        ImageList imageList = new ImageList();
        public ImageList ListImage
        {
            get { return imageList; }
            set {
                imageList = value;
                //this.Size = new System.Drawing.Size(imageList.ImageSize.Width + 1, imageList.ImageSize.Height + 1);//dragon
                State = TabState.BTN_NORMAL;
            }
        }
        private TabMenu tabIndex = 0;
        private System.ComponentModel.IContainer components = null;
        public TabMenu TabMenuIndex
        {
            get { return tabIndex; }
            set
            {
                tabIndex = value;
                //this.Text = Convert.ToString(tabIndex);
            }
        }
       
        private TabState m_State;
        public TabState  State
        {
            get { return m_State; }
            set {
                
                m_State = value;
                if (imageList != null)
                {
                    if (imageList.Images.Count > (int)m_State)
                        this.BackgroundImage = imageList.Images[(int)m_State];
                    else if (m_State == TabState.BTN_SELECT)
                        this.BackgroundImage = imageList.Images[(int)TabState.BTN_DOWN];
                }
                if (m_State == TabState.BTN_SELECT)
                    ForeColor = Color.White;
                else
                    ForeColor = Color.Black;
            }
        }

        public TabMenuButton()
        {
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        protected override void OnClick(EventArgs e)
        {
            if (m_State != TabState.BTN_SELECT)
            {
                State = TabState.BTN_SELECT;
                base.OnClick(e);
            }
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            if (m_State != TabState.BTN_SELECT) State = TabState.BTN_DOWN;
            base.OnMouseDown(mevent);
        }

        protected override void OnMouseEnter(EventArgs mevent)
        {
            if (m_State != TabState.BTN_SELECT) State = TabState.BTN_OVER;
            base.OnMouseEnter(mevent);
        }

        protected override void OnMouseLeave(EventArgs mevent)
        {
            if (m_State != TabState.BTN_SELECT) State = TabState.BTN_NORMAL;
            base.OnMouseLeave(mevent);
        }
        #region InitializeComponent
        
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.components = new System.ComponentModel.Container();
            // 
            // TabMenuButton
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FlatAppearance.BorderSize = 0;
            this.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.UseVisualStyleBackColor = false;
            this.ResumeLayout(false);

        }

        #endregion
    }
}
