﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Management;
using System.ServiceProcess;
using Microsoft.Win32;


//using Polymath.Share.ExcelHelper;
using PacketSniffer.UI;
using PacketSniffer.DataBase;
using PacketSniffer.Headers;
using Microsoft.Office.Interop.Excel;

namespace PacketSniffer.UI
{
   
    public partial class ReportForm : Form
    {
        const int LEFT_POS = 20;
        public const int MAXBUFFER = 8192;

        private byte[] byteData = new byte[MAXBUFFER];
        private byte[] byteRecData = new byte[MAXBUFFER];
        private Socket[] mainSocket;                          //The socket which captures all incoming packets
        private Socket[] recSocket;                          //The socket which captures all incoming packets
        private List<string> hostIP;

        private NetworkInterface[] nicArray;
        static List<PrevPacket> prevPackets;
        private DateTimePicker dateEnd;
        private DateTimePicker dateStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private UserButton btnSearch;
        private UserButton btnExport;
        private DataGridViewTextBoxColumn NoColumn;
        private DataGridViewTextBoxColumn DateColumn;
        private DataGridViewTextBoxColumn TimeColumn;
        private DataGridViewCheckBoxColumn IsWirelessColumn;
        private DataGridViewTextBoxColumn SourceIPColumn;
        private DataGridViewTextBoxColumn DestIPColumn;
        private DataGridViewTextBoxColumn DestinationMACColumn;
        private DataGridViewTextBoxColumn DestinationNameColumn;
        private DataGridViewTextBoxColumn NetworkType;
        private DataGridViewTextBoxColumn UploadDataColumn;
        private DataGridViewTextBoxColumn DownloadDataColumn;
        private DataGridViewTextBoxColumn ManagementPacketColumn;
        private DataGridViewTextBoxColumn DataPacketColumn;
        private DataGridViewTextBoxColumn ControlColumn;
        private DataGridViewTextBoxColumn OtherPacketColumn;
        List<string> ipAddress;

        public ReportForm()
        {
            int i = 0;
            InitializeComponent();
            
        }
         
        #region Windows Form Designer generated code

        private DataGridView tableReportList;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm));
            this.tableReportList = new System.Windows.Forms.DataGridView();
            this.dateEnd = new System.Windows.Forms.DateTimePicker();
            this.dateStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExport = new PacketSniffer.UI.UserButton();
            this.btnSearch = new PacketSniffer.UI.UserButton();
            this.NoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsWirelessColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SourceIPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DestIPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DestinationMACColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DestinationNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetworkType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UploadDataColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DownloadDataColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManagementPacketColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataPacketColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ControlColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OtherPacketColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableReportList)).BeginInit();
            this.SuspendLayout();
            // 
            // tableReportList
            // 
            this.tableReportList.AllowUserToAddRows = false;
            this.tableReportList.AllowUserToDeleteRows = false;
            this.tableReportList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableReportList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NoColumn,
            this.DateColumn,
            this.TimeColumn,
            this.IsWirelessColumn,
            this.SourceIPColumn,
            this.DestIPColumn,
            this.DestinationMACColumn,
            this.DestinationNameColumn,
            this.NetworkType,
            this.UploadDataColumn,
            this.DownloadDataColumn,
            this.ManagementPacketColumn,
            this.DataPacketColumn,
            this.ControlColumn,
            this.OtherPacketColumn});
            resources.ApplyResources(this.tableReportList, "tableReportList");
            this.tableReportList.Name = "tableReportList";
            this.tableReportList.ReadOnly = true;
            this.tableReportList.RowHeadersVisible = false;
            // 
            // dateEnd
            // 
            this.dateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dateEnd, "dateEnd");
            this.dateEnd.Name = "dateEnd";
            // 
            // dateStart
            // 
            this.dateStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dateStart, "dateStart");
            this.dateStart.Name = "dateStart";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnExport, "btnExport");
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnExport.IsRoundRect = false;
            this.btnExport.Name = "btnExport";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnSearch.IsRoundRect = false;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // NoColumn
            // 
            this.NoColumn.FillWeight = 50F;
            resources.ApplyResources(this.NoColumn, "NoColumn");
            this.NoColumn.Name = "NoColumn";
            this.NoColumn.ReadOnly = true;
            // 
            // DateColumn
            // 
            this.DateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            resources.ApplyResources(this.DateColumn, "DateColumn");
            this.DateColumn.Name = "DateColumn";
            this.DateColumn.ReadOnly = true;
            // 
            // TimeColumn
            // 
            this.TimeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            resources.ApplyResources(this.TimeColumn, "TimeColumn");
            this.TimeColumn.Name = "TimeColumn";
            this.TimeColumn.ReadOnly = true;
            // 
            // IsWirelessColumn
            // 
            this.IsWirelessColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            resources.ApplyResources(this.IsWirelessColumn, "IsWirelessColumn");
            this.IsWirelessColumn.Name = "IsWirelessColumn";
            this.IsWirelessColumn.ReadOnly = true;
            // 
            // SourceIPColumn
            // 
            this.SourceIPColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            resources.ApplyResources(this.SourceIPColumn, "SourceIPColumn");
            this.SourceIPColumn.Name = "SourceIPColumn";
            this.SourceIPColumn.ReadOnly = true;
            // 
            // DestIPColumn
            // 
            resources.ApplyResources(this.DestIPColumn, "DestIPColumn");
            this.DestIPColumn.Name = "DestIPColumn";
            this.DestIPColumn.ReadOnly = true;
            // 
            // DestinationMACColumn
            // 
            resources.ApplyResources(this.DestinationMACColumn, "DestinationMACColumn");
            this.DestinationMACColumn.Name = "DestinationMACColumn";
            this.DestinationMACColumn.ReadOnly = true;
            // 
            // DestinationNameColumn
            // 
            resources.ApplyResources(this.DestinationNameColumn, "DestinationNameColumn");
            this.DestinationNameColumn.Name = "DestinationNameColumn";
            this.DestinationNameColumn.ReadOnly = true;
            // 
            // NetworkType
            // 
            this.NetworkType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NetworkType.FillWeight = 50F;
            resources.ApplyResources(this.NetworkType, "NetworkType");
            this.NetworkType.Name = "NetworkType";
            this.NetworkType.ReadOnly = true;
            // 
            // UploadDataColumn
            // 
            resources.ApplyResources(this.UploadDataColumn, "UploadDataColumn");
            this.UploadDataColumn.Name = "UploadDataColumn";
            this.UploadDataColumn.ReadOnly = true;
            // 
            // DownloadDataColumn
            // 
            resources.ApplyResources(this.DownloadDataColumn, "DownloadDataColumn");
            this.DownloadDataColumn.Name = "DownloadDataColumn";
            this.DownloadDataColumn.ReadOnly = true;
            // 
            // ManagementPacketColumn
            // 
            this.ManagementPacketColumn.FillWeight = 50F;
            resources.ApplyResources(this.ManagementPacketColumn, "ManagementPacketColumn");
            this.ManagementPacketColumn.Name = "ManagementPacketColumn";
            this.ManagementPacketColumn.ReadOnly = true;
            // 
            // DataPacketColumn
            // 
            this.DataPacketColumn.FillWeight = 50F;
            resources.ApplyResources(this.DataPacketColumn, "DataPacketColumn");
            this.DataPacketColumn.Name = "DataPacketColumn";
            this.DataPacketColumn.ReadOnly = true;
            // 
            // ControlColumn
            // 
            this.ControlColumn.FillWeight = 50F;
            resources.ApplyResources(this.ControlColumn, "ControlColumn");
            this.ControlColumn.Name = "ControlColumn";
            this.ControlColumn.ReadOnly = true;
            // 
            // OtherPacketColumn
            // 
            this.OtherPacketColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OtherPacketColumn.FillWeight = 50F;
            resources.ApplyResources(this.OtherPacketColumn, "OtherPacketColumn");
            this.OtherPacketColumn.Name = "OtherPacketColumn";
            this.OtherPacketColumn.ReadOnly = true;
            // 
            // ReportForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateEnd);
            this.Controls.Add(this.dateStart);
            this.Controls.Add(this.tableReportList);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReportForm";
            this.Resize += new System.EventHandler(this.OnResize);
            ((System.ComponentModel.ISupportInitialize)(this.tableReportList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
         
        public void LoadSetting()
        {
            int i = 0;
            try
            {
 

 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message );
            }
            
             
        }
         
         
        void UpdatePageStatus()
        {
           
        }

        private void OnResize(object sender, EventArgs e)
        {
            tableReportList.Bounds = new System.Drawing.Rectangle(LEFT_POS,btnExport.Bottom + LEFT_POS/2, Width - LEFT_POS * 2, Height - LEFT_POS   - btnExport.Bottom);
            btnExport.Left = Width - LEFT_POS - btnExport.Width  ;
            btnSearch.Left = btnExport.Left - LEFT_POS - btnExport.Width;
        }
         
        private void btnExport_Click(object sender, EventArgs e)
        {
            if (tableReportList.Rows.Count < 1) return;

            string filename = "CrystalReport";
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.FileName = filename;
            fileDialog.Filter = "Excel files (*.xlsx)|*.xlsx"; ////////////// CMI
            DialogResult rst = fileDialog.ShowDialog();

            if (rst !=  System.Windows.Forms.DialogResult.OK) return;
            filename = fileDialog.FileName;
            DataGridView grid = tableReportList;

            try
            {
                try
                {
                    File.Delete(filename);
                }
                catch
                {
                    MessageBox.Show("File is opening. Close and retry file.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                System.Windows.Forms.Application.DoEvents();

                if (grid == null) return;

                Type officeType = Type.GetTypeFromProgID("Excel.Application");
                if (officeType == null)
                {
                    MessageBox.Show("You must install Excel to use this feature.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    ExportToXls(filename, grid);
                    ChangeExcelStyle(filename, grid);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString(), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void ExportToXls(string filename, DataGridView grid)
        {
            if (grid == null || String.IsNullOrEmpty(filename))            
                return;
            
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            Workbook workbook = excelApp.Workbooks.Add();
            Worksheet sheet = (Worksheet)workbook.Sheets[1];

            // Excel indexes are 1 based
            int coli = 1;
            foreach (DataGridViewColumn col in grid.Columns)
            {
                sheet.Cells[coli][1] = col.HeaderText;
                coli++;
            }
                        
            sheet.Rows[1].Interior.Color = Color.FromArgb(195, 190, 150);

            int rowi = 2;
            foreach (DataGridViewRow row in grid.Rows)
            {
                for (coli = 1; coli < row.Cells.Count; coli++)
                {
                    sheet.Cells[coli][rowi] = row.Cells[coli - 1].Value;
                }

                sheet.Rows[rowi].Borders.LineStyle = XlLineStyle.xlContinuous;
                sheet.Rows[rowi].VerticalAlignment = XlVAlign.xlVAlignBottom;
                sheet.Rows[rowi].HorizontalAlignment = XlHAlign.xlHAlignLeft;
                rowi++;
            }

            sheet.Cells.Columns.AutoFit();
            workbook.SaveAs(filename);
            excelApp.Quit();
            MessageBox.Show("Export to " + filename + " is successfully finished.","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        public static void ChangeExcelStyle(string fileName, DataGridView table)
        {
            string strExcelIndex = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            object missing = Type.Missing;
            Microsoft.Office.Interop.Excel.Application appXL = new Microsoft.Office.Interop.Excel.Application();
            Workbook oWB = null;
            Worksheet oSheet = null;
            Range Cells = null;
            Range titleRange = null;
            Range ContentRange = null;

            appXL.Visible = false;

            oWB = appXL.Workbooks.Open(fileName, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            oSheet = oWB.ActiveSheet as Worksheet;

            string rowEndString = strExcelIndex.Substring(table.ColumnCount - 1, 1) + Convert.ToString(table.RowCount /*+ 1*/);
            string ColEndString = strExcelIndex.Substring(table.ColumnCount - 1, 1) + "1";


            titleRange = oSheet.get_Range("A1", ColEndString);      // get title range
            ContentRange = oSheet.get_Range("A1", rowEndString);    // get Content range


            ContentRange.Borders.LineStyle = XlLineStyle.xlContinuous;
            ContentRange.VerticalAlignment = XlVAlign.xlVAlignBottom;
            ContentRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
            ContentRange.ShrinkToFit = false;

            titleRange.Interior.Color = Color.FromArgb(195, 190, 150);
            for (int index = 1; index <= table.ColumnCount; index++)
            {
                Cells = titleRange.Cells[1, index];
                Cells.ColumnWidth = table.Columns[index - 1].Width / 6;
            }

            oWB.Save();
            oWB.Close(missing, missing, missing);

            appXL.UserControl = true;
            appXL.Quit();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            List<ReportEntity> list = new List<ReportEntity>();
            list.AddRange(MonitorForm.reportList);

            foreach (ReportEntity log in list)
            {
                if (ReportDB.IsExist(log))
                    ReportDB.UpdateLogEntity(log);
                else
                    ReportDB.InsertLogEntity(log);
            }
            tableReportList.Rows.Clear();
            list = ReportDB.SearchForDate(dateStart.Value, dateEnd.Value);
            foreach (ReportEntity item in list)
            {
                tableReportList.Rows.Add(new object[]
                {
                    item.SerialNo,
                    item.Date,
                    item.Time,
                    item.IsWireless,
                    item.SourceIP,
                    item.DestinationIP,
                    item.DestinationMACAddress,
                    item.DestinationName,
                    item.Type,
                    item.UploadData,
                    item.DownloadData,
                    item.TotalPackets,
                    item.ManagementPackets,
                    item.DataPackets,
                    item.ControlPackets,
                    item.OtherPackets
                });
            }
        }
    }
}
