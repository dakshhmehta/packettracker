﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


namespace PacketSniffer.UI
{
    public class UserButton : IconButton
    {
        private ImageList imageListNormalBtn;
         
        public UserButton () : base()
        {
            InitializeComponent();
             
            this.ListImage = imageListNormalBtn;
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserButton));
            this.imageListNormalBtn = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // imageListNormalBtn
            // 
            this.imageListNormalBtn.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListNormalBtn.ImageStream")));
            this.imageListNormalBtn.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListNormalBtn.Images.SetKeyName(0, "button_normal.png");
            this.imageListNormalBtn.Images.SetKeyName(1, "button_highligh.png");
            this.imageListNormalBtn.Images.SetKeyName(2, "button_pressed.png");
            // 
            // UserButton
            // 
            this.FlatAppearance.BorderSize = 0;
            this.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ResumeLayout(false);

        }
    }
}
