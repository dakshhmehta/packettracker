﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using PacketSniffer.DataBase;

namespace PacketSniffer.UI
{
    public partial class UserInfoForm : Form
    {
        public PMUserEntity resultUser = null;
        public bool PwdChange = false;
        string oldName = "";

        public GroupBox UserInfoBox
        {
            get { return groupUser; }
            set { groupUser = value; }
        }


        public UserInfoForm()
        {
            InitializeComponent();
        }

        public bool UserResult(PMUserEntity user = null)
        {
            resultUser = user;

            if (user == null)
            {
                Text = "Add User";
                txtConfirm.Text =
                    txtPassword.Text =
                    txtName.Text = "";

                cbRoleType.SelectedIndex = 0;
            }
            else
            {
                Text = "Modify " + (EAdminRole)user.role;
                cbRoleType.SelectedIndex = user.role;
                //if (Program.LoginUser.role == (int)EAdminRole.User)
                    cbRoleType.Enabled = false;
                txtConfirm.Text =
                    txtPassword.Text = user.password;

                txtName.Text = user.pmUserName;
            }
            oldName = txtName.Text;
            lblMsgState.Text = "";
            lblCaption.Text = this.Text;
            PwdChange = false;
            if(Program.LoginUser.role == (int) EAdminRole.Administrator)
                return this.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            return true;
        }
           
        private void btnApply_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                lblMsgState.ForeColor = Color.Red;
                lblMsgState.Text = "Please input User Name!";
                txtName.SelectAll();
                this.txtName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtEmailAddress.Text))
            {
                lblMsgState.ForeColor = Color.Red;
                lblMsgState.Text = "Please input Email Address!";
                txtEmailAddress.SelectAll();
                this.txtEmailAddress.Focus();
                return;
            }
            if (txtPassword.Text == txtConfirm.Text)
            {
                if (DB01PMUser.IsExist(txtName.Text) && oldName != txtName.Text)
                {
                    lblMsgState.ForeColor = Color.Red;
                    lblMsgState.Text = "User name is exist!";
                    txtName.SelectAll();
                    this.txtName.Focus();
                    return;
                }
                bool isAdd = (resultUser == null);
                if(isAdd)
                     resultUser = new PMUserEntity();

                resultUser.pmUserName = txtName.Text;
                resultUser.role = (int)cbRoleType.SelectedIndex;
                resultUser.password = txtPassword.Text;
                resultUser.Email = txtEmailAddress.Text;
                if (isAdd)
                {
                    resultUser.LoginTime = "New " + (EAdminRole)resultUser.role;
                    resultUser.LogoutTime = "-";
                }

                //groupUser.Parent = this;
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                lblMsgState.ForeColor = Color.Red;
                lblMsgState.Text = "Password is incorrect!";
                this.txtPassword.SelectAll();
                this.txtPassword.Focus();
                return;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //groupUser.Parent = this;
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            lblMsgState.Text = "";
            PwdChange = true;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            lblMsgState.Text = "";
        }

        private void txtConfirm_TextChanged(object sender, EventArgs e)
        {
            lblMsgState.Text = "";
        }

    }
}
