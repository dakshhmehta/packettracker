﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net.Mail;

using PacketSniffer.DataBase;
using System.Net;

namespace PacketSniffer.UI
{
    public partial class RegisterForm : Form
    {
        private string verificationCode = null;
        private string password = null;

        public RegisterForm()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            // validate inputs
            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                ShowError("Missing username!");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                ShowError("Missing password!");
                return;
            }

            if (txtPassword.Text != txtPassword2.Text)
            {
                ShowError("Passwords doesn't match!");
                return;
            }

            if (String.IsNullOrEmpty(txtEmailAddress.Text))
            {
                ShowError("Missing email address!");
                return;
            }

            // check user existence
            if (DB01PMUser.IsExist(txtUserName.Text))
            {
                ShowError("This username already exists!");
                return;
            }

            password = txtPassword.Text;
            verificationCode = GeneratePassword(8);

            SmtpClient client = null;
            try
            {
                MailMessage mail = new MailMessage(Properties.Settings.Default.EmailSender, txtEmailAddress.Text)
                {
                    Subject = Properties.Settings.Default.EmailSubject,
                    IsBodyHtml = true,
                    Body = Properties.Resources.mailtemplate.Replace("@@CODE@@",verificationCode)
                };

                NetworkCredential credentials = null;
                if (!String.IsNullOrEmpty(Properties.Settings.Default.SmtpServerUserName))
                {
                    credentials = new NetworkCredential
                    {
                        UserName = Properties.Settings.Default.SmtpServerUserName,
                        Password = Properties.Settings.Default.SmtpServerPasword
                    };
                }
                
                client = new SmtpClient
                {
                    Host = Properties.Settings.Default.SmtpServerAddress,
                    Port = Properties.Settings.Default.SmtpServerPort,
                    EnableSsl = Properties.Settings.Default.SmtpUseSecureConnection,
                    Credentials = credentials
                };

                client.Send(mail);

                // mail sent, show verification box                
                txtMessage.Visible = true;                
                btnConfirm.Visible = true;
                txtMessage.ForeColor = Color.Green;
                txtMessage.Text = "A verification email is sent, please enter the code:";
                tbVerificationCode.Visible = true;
                tbVerificationCode.Focus();
                btnRegister.Enabled = false;

            }
            catch (Exception ex)
            {
                // TODO proper exception handling
                MessageBox.Show("There was an error during verification mail send: " + ex.Message, "SMTP error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ShowError("Couldn't send confirmation mail, please try again!");
            }
            finally
            {
                if (client != null)
                    client.Dispose();
            }
        }

        private string GeneratePassword(int length)
        {
            Random rnd = new Random(DateTime.Now.Millisecond);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                sb.Append(rnd.Next(0, 10));
            }

            return sb.ToString();
        }

        private void ShowError(string message)
        {
            txtMessage.Visible = true;
            txtMessage.ForeColor = Color.Red;
            txtMessage.Text = message;
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(verificationCode) && tbVerificationCode.Text.Trim() == verificationCode)
            {
                // add user
                try
                {
                    PMUserEntity newUser = new PMUserEntity
                    {
                        pmUserName = txtUserName.Text,
                        LoginTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        password = password,
                        role = (int)EAdminRole.User,
                        Email =  txtEmailAddress.Text
                    };

                    // TODO: userindex remains 0 for new users, causes malfunction on logout.
                    DB01PMUser.InsertPMUserEntity(newUser);
                    Program.LoginUser = newUser;
                }
                catch (Exception ex)
                {
                    // TODO proper exception handling
                    MessageBox.Show("There was an error during user creation: " + ex.Message, "DB error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                MessageBox.Show("Registration was successful!","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Confirmation code is not valid, please try it again!", "Invalid code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.DialogResult = DialogResult.None;
            }
        }
    }
}
