﻿namespace PacketSniffer.UI
{
    partial class UserInfoForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserInfoForm));
            this.groupUser = new System.Windows.Forms.GroupBox();
            this.lblMsgState = new System.Windows.Forms.Label();
            this.cbRoleType = new System.Windows.Forms.ComboBox();
            this.btnCancel = new PacketSniffer.UI.UserButton();
            this.lblCaption = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtConfirm = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnApply = new PacketSniffer.UI.UserButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmailAddress = new System.Windows.Forms.TextBox();
            this.groupUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupUser
            // 
            this.groupUser.Controls.Add(this.label3);
            this.groupUser.Controls.Add(this.txtEmailAddress);
            this.groupUser.Controls.Add(this.lblMsgState);
            this.groupUser.Controls.Add(this.cbRoleType);
            this.groupUser.Controls.Add(this.btnCancel);
            this.groupUser.Controls.Add(this.lblCaption);
            this.groupUser.Controls.Add(this.label1);
            this.groupUser.Controls.Add(this.label2);
            this.groupUser.Controls.Add(this.lblPassword);
            this.groupUser.Controls.Add(this.lblUsername);
            this.groupUser.Controls.Add(this.txtName);
            this.groupUser.Controls.Add(this.txtConfirm);
            this.groupUser.Controls.Add(this.txtPassword);
            this.groupUser.Controls.Add(this.btnApply);
            resources.ApplyResources(this.groupUser, "groupUser");
            this.groupUser.Name = "groupUser";
            this.groupUser.TabStop = false;
            // 
            // lblMsgState
            // 
            resources.ApplyResources(this.lblMsgState, "lblMsgState");
            this.lblMsgState.BackColor = System.Drawing.Color.Transparent;
            this.lblMsgState.ForeColor = System.Drawing.Color.Red;
            this.lblMsgState.Name = "lblMsgState";
            // 
            // cbRoleType
            // 
            this.cbRoleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRoleType.FormattingEnabled = true;
            this.cbRoleType.Items.AddRange(new object[] {
            resources.GetString("cbRoleType.Items"),
            resources.GetString("cbRoleType.Items1")});
            resources.ApplyResources(this.cbRoleType, "cbRoleType");
            this.cbRoleType.Name = "cbRoleType";
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCancel.IsRoundRect = false;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblCaption
            // 
            resources.ApplyResources(this.lblCaption, "lblCaption");
            this.lblCaption.BackColor = System.Drawing.Color.Transparent;
            this.lblCaption.Name = "lblCaption";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblPassword
            // 
            this.lblPassword.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblPassword, "lblPassword");
            this.lblPassword.Name = "lblPassword";
            // 
            // lblUsername
            // 
            this.lblUsername.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblUsername, "lblUsername");
            this.lblUsername.Name = "lblUsername";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtConfirm
            // 
            resources.ApplyResources(this.txtConfirm, "txtConfirm");
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.TextChanged += new System.EventHandler(this.txtConfirm_TextChanged);
            // 
            // txtPassword
            // 
            resources.ApplyResources(this.txtPassword, "txtPassword");
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // btnApply
            // 
            resources.ApplyResources(this.btnApply, "btnApply");
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.FlatAppearance.BorderSize = 0;
            this.btnApply.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnApply.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnApply.IsRoundRect = false;
            this.btnApply.Name = "btnApply";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txtEmailAddress
            // 
            resources.ApplyResources(this.txtEmailAddress, "txtEmailAddress");
            this.txtEmailAddress.Name = "txtEmailAddress";
            // 
            // UserInfoForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupUser);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserInfoForm";
            this.groupUser.ResumeLayout(false);
            this.groupUser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupUser;
        private System.Windows.Forms.ComboBox cbRoleType;
        private UserButton btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtConfirm;
        private System.Windows.Forms.TextBox txtPassword;
        private UserButton btnApply;
        private System.Windows.Forms.Label lblMsgState;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmailAddress;
    }
}
