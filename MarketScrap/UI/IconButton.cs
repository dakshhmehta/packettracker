﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PacketSniffer.UI
{
    public partial class IconButton : Button 
    {
        protected  System.ComponentModel.IContainer components = null;
        public ImageList ListImage
        {
            set {
                imageList = value;
                this.Width = value.ImageSize.Width+1;
                this.Height=value.ImageSize.Height+1;
                this.Image = imageList.Images[0];
            }
        }
        private bool isRoundRect;
        public bool IsRoundRect
        {
            get { return isRoundRect; }
            set { isRoundRect = value ; }
        }
        public IconButton()
        {

            InitializeComponent();

            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }
        public IconButton(ImageList imagelist)
        {

            InitializeComponent();
            ListImage  = imagelist;
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        protected override void OnClick(EventArgs e)
        {
            if (imageList.Images.Count > 0)
                this.Image=imageList.Images[0];
            base.OnClick(e);
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            if (imageList.Images.Count > 2)
                this.Image = imageList.Images[2];
            base.OnMouseDown(mevent);
        }

        protected override void OnMouseEnter(EventArgs mevent)
        {
            if (imageList.Images.Count > 1)
                this.Image = imageList.Images[1];
            base.OnMouseEnter(mevent);
        }

        protected override void OnMouseLeave(EventArgs mevent)
        {
            if(imageList.Images.Count > 0)
                this.Image = imageList.Images[0];
            base.OnMouseLeave(mevent);
        }
        protected override void OnEnabledChanged(EventArgs e)
        {
            if (this.IsRoundRect)
            {
                this.Image = null;
                this.BackgroundImage = imageList.Images[0];
            }

            //if (Enabled)
            //    ForeColor = System.Drawing.Color.Black;
            //else
            //{
            //    ForeColor = System.Drawing.Color.Gray;
            //    //if (imageList.Images.Count > 3)    this.Image = imageList.Images[3];
            //}
            base.OnEnabledChanged(e);
        }
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IconButton ));
            this.components = new System.ComponentModel.Container();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();       
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // IconButton
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.FlatAppearance.BorderSize = 0;
            this.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Size = new System.Drawing.Size(33, 33);
            this.UseVisualStyleBackColor = false;
            this.ResumeLayout(false);

            
        }
        private ImageList imageList;
    }
}
