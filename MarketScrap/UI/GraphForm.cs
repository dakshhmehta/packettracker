﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;


namespace PacketSniffer.UI
{
    public partial class GraphForm : Form
    {
        const int LEFT = 25;
        const int step = 5;
        const int LABEL_COUNT = 5;
        long max = 0;

        long ULRate = 0;
        long DLRate = 0;
        long prevULRate = 0;
        long prevDLRate = 0;
        private NetworkInterface[] nicArray;
        private PrevPacket[] prevPackets;
        private Label[] lblYs;
        List<Rates> rateList;
        int time = 0;
        Graphics g;

        public GraphForm()
        {
            InitializeComponent();

            nicArray = NetworkInterface.GetAllNetworkInterfaces();
            rateList = new List<Rates>();
            prevPackets = new PrevPacket[nicArray.Length];
            for (int i = 0; i < nicArray.Length; i++)
            {
                UnicastIPAddressInformationCollection ipInfo = nicArray[i].GetIPProperties().UnicastAddresses;
                foreach (UnicastIPAddressInformation info in ipInfo)
                    if (info.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        prevPackets[i].Address = info.Address.ToString();
                        break;
                    }
                 
                IPv4InterfaceStatistics data = nicArray[i].GetIPv4Statistics();
                prevPackets[i].ReceivedBytes = data.BytesReceived;
                prevPackets[i].SentBytes = data.BytesSent;
            }
            
            lblYs = new Label[LABEL_COUNT + 1];
            for (int i = 0; i <= LABEL_COUNT; i++)
            {
                lblYs[i] = new Label();
                this.lblYs[i].AutoSize = true;
                this.lblYs[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.lblYs[i].Name = "lblUnit";
                this.lblYs[i].TabIndex = 10 + i;
                this.lblYs[i].Parent = groupGraph;
                this.lblYs[i].Visible = true;
            }

            cbMaxSpeed.SelectedIndex = 4;
            g = panelGraph.CreateGraphics();

        }

        protected override void OnResize(EventArgs e)
        {

            groupRateValue.Left = LEFT;
            groupRateValue.Width = Width - LEFT * 2;

            groupGraph.Bounds = new Rectangle(LEFT, groupRateValue.Bottom + LEFT, Width - LEFT * 2, Height - groupRateValue.Bottom - LEFT *   2 );
            panelGraph.Bounds = new Rectangle(lblUL.Left, lblUL.Bottom + LEFT  , groupGraph.Width - lblUL.Left * 2, groupGraph.Height - lblUL.Bottom - LEFT * 2);
            
            RefreshLabel();

            panelGraph.Invalidate();
        }

        public void UpdateForm()
        {
            try
            {
                prevDLRate = DLRate;
                prevULRate = ULRate;
                ULRate = 0;
                DLRate= 0;
                for (int i = 0; i < nicArray.Length; i++)
                {
                    IPv4InterfaceStatistics data = nicArray[i].GetIPv4Statistics();
                    DLRate += (data.BytesReceived - prevPackets[i].ReceivedBytes);
                    ULRate += (data.BytesSent - prevPackets[i].SentBytes);
                    

                    prevPackets[i].ReceivedBytes = data.BytesReceived;
                    prevPackets[i].SentBytes = data.BytesSent;
                }

                txtDLRate.Text = MonitorForm.GetSpeed(DLRate);
                txtULRate.Text = MonitorForm.GetSpeed(ULRate);
                
                time+=step;
                rateList.Add(new Rates(time, ULRate, DLRate));

                if (time > panelGraph.Width)
                {
                    for (int i = 1; i < rateList.Count; i++)
                    {
                        rateList[i].time -= time - panelGraph.Width;
                    }
                    rateList.RemoveAt(0);
                    time = panelGraph.Width;
                    //panelGraph.Invalidate(panelGraph.ClientRectangle);
                    panelGraph.Invalidate();
                }
                else
                    panelGraph.Invalidate(new Rectangle(time -step, 0,   step,panelGraph.Height));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public float Value(long speed)
        {
            int temp = 1;
            float res = speed * (panelGraph.Height - temp) / max + temp;
            
            res = panelGraph.Height - res;
            return res;
        }

        private void panelGraph_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            int y;
            for (int i = 1; i < LABEL_COUNT; i++)
            {
                y = (LABEL_COUNT - i) * panelGraph.Height / LABEL_COUNT;
                ControlPaint.DrawBorder(g, new Rectangle(-1, 0, panelGraph.Width + 1, y), Color.Gray, ButtonBorderStyle.Dashed);
            }
            y = 1;
            if (e.ClipRectangle != panelGraph.ClientRectangle)
            {
                y = rateList.Count - 2;
                if (y < 1)
                    y = 1;
            }
            for (int i = 1; i < rateList.Count; i++)
            {
                g.DrawLine(Pens.Green, rateList[i].time, Value(rateList[i].DLRate), rateList[i].time - step, Value(rateList[i - 1].DLRate));
                g.DrawLine(Pens.Blue, rateList[i].time, Value(rateList[i].ULRate), rateList[i].time - step, Value(rateList[i - 1].ULRate));
            }
            
        }

        private void cbMaxSpeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            max = Convert.ToInt64(cbMaxSpeed.Text) * 1024;

            RefreshLabel();
            
            panelGraph.Invalidate();
        }

        private void RefreshLabel()
        {
            if (lblYs != null)
            {
                for (int i = LABEL_COUNT; i >= 0; i--)
                {
                    float vv = (float)i * (float)max / 1024.0f / (float)LABEL_COUNT;
                    this.lblYs[i].Text = vv.ToString("0.0");
                    this.lblYs[i].Location = new System.Drawing.Point(panelGraph.Left - lblYs[i].Width, (LABEL_COUNT - i) * panelGraph.Height / LABEL_COUNT - lblYs[i].Height / 2 + panelGraph.Top);
                }
            }
        }
 
    }
}
